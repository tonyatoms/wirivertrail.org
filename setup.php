<?php

	// DB connection parameters
	define('DB_HOST', 'localhost');
	define('DB_NAME', 'atomsnet_wirivertrail_dev');
	define('DB_USER', 'atomsnet_wrt');
	define('DB_PASSWORD', 'L3T-78h-r00');
	
	// app URL NOTE: include the subdirectory if applicable, leave off the trailing /
	define('APP_URL', 'http://atoms.net/wirivertrail-dev');
	
	// setup default language for the site
	define('DEFAULT_LANGUAGE', 'en');
	
	// site admin
	define('SITE_ADMIN', 'tony@atoms.net');
	
	// passcode
	define('PASSCODE', 'iloverespond');
	
	// CORS (optional for external sites)
	define ('CORS', serialize (array (
	    'http://path.torespond.com'
	    )));
	    
	// Google Maps API Key - this is a GOOGLE api key from 2006 may not be valid or correct for maps
	define('GOOGLE_MAPS_API_KEY', 'AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY');
	
	// - Stripe
    // - set to the Stripe plan you want the user enrolled in when the site is created
    // - create account and plans at stripe.com (a trial period is recommended)
    define('DEFAULT_STRIPE_PLAN', '');
    
    // set Stripe API keys 
	define('STRIPE_API_KEY', '');
    define('STRIPE_PUB_KEY', '');
    
    // set what emails should be sent out and a reply-to email address
	define('REPLY_TO', '');
	define('SEND_WELCOME_EMAIL', false);
	define('SEND_PAYMENT_SUCCESSFUL_EMAIL', false);
	define('SEND_PAYMENT_FAILED_EMAIL', false);
	
    // start page (sets the default page a user sees after logon)
	define('START_PAGE', 'pages');
	
	// set the default theme (directory name: themes/simple => simple)
	define('DEFAULT_THEME', 'simple');

	// the brand of your app
    define('BRAND', 'Wisconsin River Trail Project');
    define('COPY', '<a href="http://respondcms.com">Respond CMS</a> version '.VERSION.'.  Made by Matthew Smith in Manchester, MO');

?>