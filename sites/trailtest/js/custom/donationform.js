  <script src= "http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

      <script>

        $('.input-group').button();

        var $amount = 0;  
        var $amountFormatted = ''; 
        var checkboxVal = 'off';    
        var checkboxValFormatted = 'One Time Donation $';    

        function checkboxChanged(){

            validateInput();

            checkboxVal = document.getElementById("subscriptionCbox").value;
        }

        function amountChanged(){

            $amount =  $( "#amount" ).val() ;  

            validateInput();
        }
        function validateInput(){
            $('#inputForm').validate({
                debug: true,
                rules: {
                    amount: {
                        required: true,
                    },
                },
                messages: {
                    amount: {
                        required: " A dollar amount for your donation is required.",
                }
            }
         });
            if( $amount > 0  && (! isNaN($amount) )) {
                $amountFormatted = $amount + '.00';
                if($("#subscriptionCbox").is(':checked')) {
                    checkboxValFormatted = 'Monthly Subscription of $'; 
                }
                else {
                    checkboxValFormatted = 'One time Donation: $';
                }
                $('#inputError').hide();
                $('#readyButtonDiv').hide();
                $('#checkoutDiv').show();
            }
            else {
                $('#readyButtonDiv').show();
                $('#checkoutDiv').hide();
                $('#inputError').show();
            }        
        }      
        </script> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Donate!</h1>

    <form id="inputForm" class="well" action='' method="POST">  

        <label>Donation Amount  $</label>  

        <input class="span3" placeholder="100" type="text" onblur="amountChanged()" id="amount" name="amount"  required>
        <div hidden="true" id="inputError" style="color:red; margin-left:60px" >please enter a whole dollar amount only</div>  

        <label class="checkbox">  
        <input type="checkbox" id="subscriptionCbox" onchange="checkboxChanged()"> Make this a recurring (monthly) donation! 
        </label>     

        <div id="readyButtonDiv">     
            <button type="button" class="btn btn-primary">
                <span class="glyphicon glyphicon-ok"></span> next
            </button>
        </div> 

        <script src="https://checkout.stripe.com/checkout.js"></script>    

        <div id="checkoutDiv"   hidden="true">    
            <button id="customButton">Donate!</button>
        </div>
        <script>
            var handler = StripeCheckout.configure({
                key: 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
                image: "t_wrtp_demo5.jpg",
                token: function(token, args) {
          // Use the token to create the charge with a server-side script.
          // You can access the token ID with `token.id`
                }
            });

            document.getElementById('customButton').addEventListener('click', function(e) {
                // Open Checkout with further options
                handler.open({
                    name: 'River Trail Project',
                    description: checkboxValFormatted + $amountFormatted ,
                    amount: $amount * 100
                });
                e.preventDefault();
            });
        </script>        
    </form>     