<?php 
$rootPrefix="../";
$pageUrl="post/wisconsin-river-trail";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="53fa9dec47d37";
$pageFriendlyId="wisconsin-river-trail";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - WISCONSIN RIVER TRAIL</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="53fa9dec47d37" data-pagefriendlyid="wisconsin-river-trail" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="wisconsin-river-trail">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1408933363"><?php print _("WISCONSIN RIVER TRAIL"); ?></h1><p id="p-1408933363"><?php print _("August 24, 2014"); ?></p><p id="1408933550"><?php print _("We had a wonderful turnout of local children and volunteers at the 1st annual back-to-school bike safety event.  It was a lot of fun, AND our kids learned how to make sure they’re safe on their bikes.  Door prizes, hot dogs, and ice cream were enjoyed by all.  We would like to thank the many volunteers that it took to make this a hit-including the help of the Gunderson Boscobel Hospital and Clinic staff, the fire department, and our own certified bike safety trainer, Mac McMullen from Spring Green.  A special thank you to the parents for seeing the importance of bike safety!  "); ?></p><p id="1408933550"><?php print _("What is the next major hoopla, you might be wondering?  November 1st, we will be having the Hiking and Biking Unlimited Banquet.  Tickets are $25.00, which includes a wonderful meal and a chance to win some awesome door prizes sponsored by area businesses. There will be a live and silent auction filled with great holiday gift items, as well as raffles.   If you would like to reserve your tickets now, seating is limited.  We’re planning on having a big hoot of a time, and we’d love to have you come enjoy the evening with us.   Please contact us at wisrivertrail @ gmail.com, or send us a message on our facebook site, Wisconsin River Trail Project to reserve tickets.  Anyone with local talents, please contact us if you would like to donate any of your items to our cause.  "); ?></p><p id="1408933550"><?php print _("The beginning of September, we will be meeting with the UW-Platteville Engineering department to kick off their involvement in our trail.  We are more than excited to have their assistance in our project.  We will receive notification by September 1st as to whether the UW-Madison Architecture program accepts us as a Capstone project.  "); ?></p><p id="1408933550"><?php print _("The big bike that is parked on Hwy. 61 near Dairy Queen will be available in two weeks for “rent” to help celebrate any event that needs a little attention!  The cost is $15.00 to put it in a yard, $15.00 to remove it FROM your yard, and $10.00 if it’s in YOUR yard and you would like to move it to another yard.  Thanks again to Jake Bacon, Baker Mill Works, and Roddy Dull for making this bike fundraiser a reality!  Please send us a message using the above email account to reserve your big bike rental today! "); ?></p><p id="1408933550"><?php print _("Thanks for your continued support!"); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Mon, Aug 25 14 04:25 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/wisconsin-river-trail" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>