<?php 
$rootPrefix="../";
$pageUrl="post/wrt-news--april-24-2015";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="553fa5c1c60bb";
$pageFriendlyId="wrt-news--april-24-2015";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - WRT News - April 24, 2015</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="553fa5c1c60bb" data-pagefriendlyid="wrt-news--april-24-2015" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="wrt-news--april-24-2015">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1430234565"><?php print _("WRT News - April 24, 2015"); ?></h1><p id="p-1430234565"><?php print _("I know….I know….I missed my
column last week.  But, hey!  The Price Is Right was calling!  Unfortunately my sister and I were just in
the audience, but we sure had a fun day! 
June 11th is our air date, so look for us in the third row!<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("Now on to Trail News!   Our FIRST annual yearly meeting is going to
be held the end of May.  (We’re working
on dates that all the board members can come!) 
We plan on adding new board members and reviewing our year’s progress,
as well as detailing what our next year of plans and fundraising will
entail.  The meeting will be held at 1404
Wisconsin Avenue.  We really want to
encourage all who are interested in this project to attend.  Many hands make light work, and that is true
for the WRTO.  We’re a pretty fun bunch
to hang out with!  If you are not sure if
you’re interested or not, come anyway. 
We love to see new faces!  We will
know the exact date by next week.<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("We will be sponsoring a bus trip
to The Brewers game with the Goo Goo Dolls concert afterward on Saturday,
August 15th.  The bus leaves at 12:00,
game is at 6 pm, and the concert will last about one hour after the game.  Price of the tickets and the bus ride is
$82.00.  Call 507-269-2606 for more info
or email Denise at denisefisher5995@gmail.com. 
This is going to be a BLAST!<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("The Boscobel Farmer’s Market is
a couple of weeks from becoming a reality! 
Yeah!  The WRTO is sponsoring a
booth, and will sell baked goods, plants, clothing, and merchandise, as well as
sell tickets to the Rooster Andy’s 3rd of July Chicken BBQ in
Kronshage Park.  Last year we sold 500
tickets and this year we hope to sell 600! 
Please support our local area vendors and stop in and say hi to us at
our booth!<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("Mother’s Day is coming up and
don’t forget to stop by the Timber Lane Coffee Shop for your chance to win one
of two Mother’s Day baskets!  One basket
is a wine basket, and the other is a coffee basket, donated by Marianne and
Karl Krogen and Madge Stuart and Dr. Pelz. 
The winners will be drawn on May 8th.  Tickets are just $2/ticket or 6 tickets/$10.<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("Our
big green bike is rentable for all your events, including graduations,
anniversaries, birthdays, or just to embarrass! 
Please call Joel at 375-5708 to reserve it.  The cost is $15 to put it in a yard.  If you want to move it out of your yard, it’s
$10 to move it to someone else’s yard, but $15 to just have it removed from
your yard (without putting it in your friend’s yard). "); ?></p><p id="1430234925"><?php print _("We
are so looking forward to June 6th from 9-noon at the Boscobel Fire
Department.  We are teaming up with the
Boscobel Fire Department, Gunderson Boscobel Hospital and Clinics, Children’s
Miracle Network, Wisconsin River Outings, and WRTO to provide
a FREE bike safety training day with our certified trainer!  What’s great is we are supplying snacks for
the kids-hot dogs, chips, granola bars, waters, and treats.  We will also have great door prizes.  Those signed up by May 22nd will
be entered into a free drawing for WRTO t-shirts and a free canoe rental!  Please email at <a href=\"mailto:denisefisher5995@gmail.com\">denisefisher5995@gmail.com</a> or <a href=\"mailto:tmvial@yahoo.com\">tmvial@yahoo.com</a>, or call 507-269-2606 or
608-391-0113 to be sent a registration form/permission slip.  "); ?></p><p id="1430234925"><?php print _("Clothing and merchandise are now
in at World of Variety!  Go check out our
selection!<o:p></o:p>"); ?></p><p id="1430234925"><?php print _("Hope everyone has a great, great
week!  Thanks for supporting the
Wisconsin River Trail!  We’ll be digging
in the ground by fall of this year!  <o:p></o:p>"); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Mactire McMullen</span>
        <span class="last-modified-date">Tue, Apr 28 15 10:28 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/wrt-news--april-24-2015" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>