<?php 
$rootPrefix="../";
$pageUrl="post/wisconsin-river-trail-org";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="53b356591478f";
$pageFriendlyId="wisconsin-river-trail-org";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Wisconsin River Trail Organization</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="53b356591478f" data-pagefriendlyid="wisconsin-river-trail-org" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="wisconsin-river-trail-org">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1404261980"><?php print _("Wisconsin River Trail Organization"); ?></h1><p id="p-1404261980"><?php print _("June 28, 2014"); ?></p><p id="1404262096"><?php print _("Have we told you about our Rooster Andy’s Chicken BBQ Welcome Home Tailgating Party?  It’s almost here!  "); ?></p><p id="1404262096"><?php print _("Gather up your family and friends and come help us celebrate our 1st get together!  We’re so excited to sponsor this event at the Kronshage Park in Boscobel from 5-9 pm.  The BBQ will be served from 5-7 pm by Rooster Andy’s from LaCrosse.  The meal consists of ½ chicken, potato salad, beans, roll, drink, and dessert for $10.00.  There will be a DJ playing our favorite tunes, face painting for the kids, Schwan’s ice cream treats for sale, T-shirts and bracelets for sale, as well as drawings for prizes.  We will have 52-$10. chances on a $450 bike from Blue Dog Cycle in Viroqua.  Deliveries are available by calling 485-1202 (Wendi Stitzer) or 391-0113 (Tonia Vial).   If you plan on coming but haven’t picked up a ticket, please let us know by a text, phone call, or posting on our Facebook group page, Wisconsin River Trail Project, that you are coming.  We want to make sure we have enough meals ordered for everyone, so please let us know ahead of time.  The 4th of July weekend is my favorite time of year.   "); ?></p><p id="1404262096"><?php print _(" The 4th of July we will have Schwan’s ice cream treats for sale at the drive thru entrance of Community First Bank.  We also have a great float courtesy of Jake Bacon and Roddy Dull.  If you would like to ride your bikes or walk in the 4th with your family or friends, we have spot #24 at the parade line up.  Please meet us there and show your support for our Trail. "); ?></p><p id="1404262096"><?php print _("  Our brochure is ready for print!  Chris Stauffer’s team at LaCrosse Gunderson Lutheran Hospital has done an outstanding job creating the brochure that we will have ready to hand out during the festivities over the 3rd and 4th of July.  This brochure explains our purpose, our thoughts on where our trail will roughly be, and our time frame for completing it. "); ?></p><p id="1404262096"><?php print _("  A huge thank you goes out to Grant County Chapter of Thrivent Financial for their generous donation.  They are planning on matching funds for our chicken BBQ up to $250.00.  Another huge thank you to Unique Café for their donation and for the support that Doyle Lewis and the Unique team have shown to our Trail.  Thanks to Fennimore’s Kwik Trip for donating cases of water, American Signs, Superior Trophies, Boscobel Dial, Jake Bacon and Baker Iron Works, Boscobel Fire Department, and Casey’s gas station for ice donation.  I’m sure I’ve forgotten some people and businesses, but I promise to thank you over and over again.  Your support is what is going to make this trail a reality.  We couldn’t do it without you all! "); ?></p><p id="1404262096"><?php print _("In closing, I can’t tell you how excited I am about our chicken BBQ.  To come home for two short days is worth it….worth meeting up with friends and family.  I hope to see you on the 3rd….and meet you on the trail one day too! Denise Fisher Denisefisher5995 @ gmail.com Wirivertrail.org 507-269-2606   "); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Wed, Jul 02 14 02:48 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/wisconsin-river-trail-org" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>