<?php 
$rootPrefix="../";
$pageUrl="post/building-a-better-boscobe1";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="5371204788621";
$pageFriendlyId="building-a-better-boscobe1";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Building a Better Boscobel May 2, 2014</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="5371204788621" data-pagefriendlyid="building-a-better-boscobe1" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="building-a-better-boscobe1">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1399922764"><?php print _("Building a Better Boscobel May 2, 2014"); ?></h1><p id="p-1399922764"><?php print _("Hello there! Another week has flown by with some positive strides taking place. Here’s what’s been happening on the hiking and biking trail."); ?></p><p id="1399923246"><?php print _("Early next week, we will find out if our project has been accepted for this fall by the UW-Platteville Engineering Department. We are so hopeful that the department sees our need to press forward and not wait until spring. "); ?></p><p id="building-a-better-boscobe1-paragraph-2"><?php print _("Our website is coming along beautifully! If you have rural pictures of our area, bluffs, valleys, or river views, please feel free to email them to me and we will choose some to place on our website."); ?></p><p id="building-a-better-boscobe1-paragraph-3"><?php print _("Our logo has been posted on our Facebook group page, Wisconsin River Trail Project. We may need to tweak it a little and add a hiker, but it is fabulous, thanks to Roddy Dull!"); ?></p><p id="building-a-better-boscobe1-paragraph-4"><?php print _("The biggest feat of the week is that our donation department is up and running! J Please feel free to email me or go to our face book group page if you want to donate once, or, to make it less taxing on your family, donate a smaller amount bi-weekly or monthly. For any information or to be mailed a form, please contact Jo Sommers, treasurer, at 375-4117 or me, at 507-269-2606."); ?></p><p id="building-a-better-boscobe1-paragraph-5"><?php print _("We booked the Boscobel Bowl and Banquet facility for our Hiking and Biking Unlimited Banquet! It will be November 1st. If you have a talent creating anything and want to contribute anything to our cause,  please feel free to contact Tonia Vial (tmvial @ yahoo.com), Wendi Stitzer (wendistitzer @y ahoo.com), or myself at any time between now and then. "); ?></p><p id="building-a-better-boscobe1-paragraph-6"><?php print _("We are planning on an evening of fun for the whole family on July 3rdtailgating party at the Kroshage Park, complete with a DJ and karaoke. We are open for any ideas of fun that will make this party exciting and entertaining for your family."); ?></p><p id="building-a-better-boscobe1-paragraph-7"><?php print _("We will be raffling off a $450. Bike on the 4th of July. of July. We are purchasing this bike from Viroqua’s BlueDog Cycle Bike Shop. We will also have helmets and water bottles for sale."); ?></p><p id="building-a-better-boscobe1-paragraph-9"><?php print _("Plans are moving forward and we are so excited to join together in friendship and fun while we are on this mission together. I, myself, look forward to enjoying old friends and meeting new friends while we continue on our journey….down our trail. "); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Mon, May 12 14 09:34 pm</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/building-a-better-boscobe1" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>