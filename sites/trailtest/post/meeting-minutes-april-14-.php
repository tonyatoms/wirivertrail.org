<?php 
$rootPrefix="../";
$pageUrl="post/meeting-minutes-april-14-";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="53593fe3de8ba";
$pageFriendlyId="meeting-minutes-april-14-";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Meeting Minutes April 14,  2014</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="53593fe3de8ba" data-pagefriendlyid="meeting-minutes-april-14-" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="meeting-minutes-april-14-">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1398357998"><?php print _("Meeting Minutes April 15,  2014"); ?></h1><p id="meeting-minutes-april-14--paragraph-3"><?php print _("Denise welcomed everyone!  We were asked to write our name, e-mail address, and phone on an attendance sheet."); ?></p><p id="meeting-minutes-april-14--paragraph-4"><?php print _("<b>Mission Statement</b> Denise shared a list of mission statements for the group to review.  Because we are a non-profit project/organization, we need to have a mission statement in place.  Everyone agreed on number 9:  “Creating an environment of health, wellness, and nature to stimulate mind, body, and soul.” Suggestion was made to form a sub-committee to make a decision on the mission statement.  After some discussion, the group came up with the following statement: <b><i>“Through the beauty of our nature trails, we are promoting an environment of health and wellness to stimulate mind, body, and soul.” </i></b>"); ?></p><p id="1398358815"><?php print _("<b>Articles of Incorporation</b> The IRS requires that we have Articles of Incorporation.  Denise will send this out for the Board to review. "); ?></p><p id="meeting-minutes-april-14--paragraph-6"><?php print _("<b>How Are We Funding This? - </b>if someone asks you this, please don’t say, “I don’t know.”  We are applying for grants with the DNR and Department of Transportation.  Contact Denise or another committee member to address questions also."); ?></p><p id="meeting-minutes-april-14--paragraph-7"><?php print _(" <b>Goals:</b> First step is to complete the Sanders Creek walkway out to the boat landing.   Second step is to create a non-motorized trail through the bluffs and river ways for hiking/biking/cross-country skiing.  This trail would run from Boscobel to Woodman.  The DNR has these two projects on their ‘master plan’ for the Lower Wisconsin River Way.  However, we need support and commitment to complete the projects in a timely manner. Denise has talked with engineers to build/plan bridge(s).  She has also been in contact with UW-Madison Architecture.  Southwest Technical College would like to help with a monument (stone), benches, and brochures."); ?></p><p id="meeting-minutes-april-14--paragraph-8"><?php print _(" <b>Plan to Attend - </b>DNR will be at the Tuffley Center on April 23rd, from 4 to 7.  They are working on their master plan and they are all for these projects.  There is DNR land that could be used for the trail between here and Woodman.  We are able to place a trail 51ft. from the railways.  Matt Seguin, Manager of the Lower Wisconsin State Riverway, will be facilitating this meeting. "); ?></p><p id="meeting-minutes-april-14--paragraph-9"></p><p id="paragraph-9"><?php print _("Denise mentioned that a Stripe Account will be set up for donations, purchase merchandise, etc.  This account will be similar to a PayPal account.  We need to pitch this to people/supporters, automatic deductions can be set up from paychecks/accounts. We have to have in our plan who is going to maintain and take care of the trail(?) Fundraising will be huge! "); ?></p><h3 id="meeting-minutes-april-14--h3-8"><?php print _("Ideas for fundraising over the July 4th holiday:"); ?></h3><ul id="ul-9"><li><?php print _("helmets "); ?></li><li><?php print _("t-shirts"); ?></li><li><?php print _("water bottles"); ?></li><li><?php print _("penny wars"); ?></li><li><?php print _("run/walk"); ?></li><li><?php print _("50/50 raffle"); ?></li><li><?php print _("deliver pizzas one night a month"); ?></li><li><?php print _("rubber bracelets"); ?></li><li><?php print _("grade-a-thon"); ?></li><li><?php print _("calendar"); ?></li></ul><h3 id="meeting-minutes-april-14--h3-10"><?php print _("Other/wrap up "); ?></h3><p id="paragraph-10"><?php print _("Bacon has offered to build/create a very large metal bike.  The bike could be placed in someone’s yard (birthday or other event), would also have to pay a fee to have it moved.  The bike could be part of the July 4th parade. Great idea, Denise! Roddy Dull has created a map for safe bike trails along the highways, from Fennimore – to Boscobel – to Woodman.  He would be willing to place historical markers and signs for the bike trails.  He walked the area from Boscobel to Woodman and found there would be little wetland area that would have to be filled in.  We could obtain placards down the road to designate flora, etc. Denise asked for volunteers/committee help:  Joel Leonard, Angie Sailor, Joe Sommers, Tanya Vial, Wendi Stitzer, and Mactire McMullen.  Roddy is more than willing to help, not able to serve on the Board or a Committee at this time.  Marianne is also willing to help where needed. If the bike trail does not happen, money that is raised could go toward the safe bike trails along the highways."); ?></p><p id="meeting-minutes-april-14--paragraph-10"><?php print _(" Mactire McMullen offered the following as an option for the mission statement: “To promote the non-motorized use of our community’s natural trails to the health conscious public.”   “Hike/Bike Wisconsin Trails”  Discover Wisconsin McNamee Property (name of hiking area)"); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Thu, Apr 24 14 07:31 pm</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/meeting-minutes-april-14-" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>