<?php 
$rootPrefix="../";
$pageUrl="post/wrt-news--may-18-2015";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="5559e7720dae7";
$pageFriendlyId="wrt-news--may-18-2015";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - WRT News - May 18, 2015</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="5559e7720dae7" data-pagefriendlyid="wrt-news--may-18-2015" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="wrt-news--may-18-2015">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1431955319"><?php print _("WRT News - May 18, 2015"); ?></h1><p id="p-1431955319"><?php print _("Next Wednesday, May 27th is our Second Annual
board meeting!  We’ve accomplished so
many things this past year and look forward to all the fun events and forward
progress we will make next year.  We are
in need of more volunteerism and board members, and if you are interested in
seeing this trail become a reality (sooner rather than later), then please make
time to come to our meeting.  Many hands
make light work, and we always have fun! 
Come see what we’re all about…we promise that we will not expect more of
you than what you are willing to give. 
Stop by at 7:00 pm at 1404 Wisconsin Avenue in Boscobel.<o:p></o:p>"); ?></p><p id="1431956002"><?php print _("Last
Saturday at the Boscobel Farmer’s Market may have been a little wet, but sure
was fun.  The WRTO has a booth where we
sell baked goods, tickets to upcoming events, have fliers of what we’re up to
for fun, and are willing to talk “Trail Talk” with anyone interested.  If you are able to donate any baked goods,
please call 507-269-2606.  It was great
to get to visit with so many familiar faces and meet new people too."); ?></p><p id="1431956002"><?php print _(" Is anyone
interested in forming a women’s bike club where we could meet at a designated
spot daily?  It would be more of a
beginners group and could start a fun new exercise program for all involved.  Please call Denise at 507-269-2606 if you are
interested in meeting for a ride!<o:p></o:p>"); ?></p><p id="1431956002"><?php print _("     You can donate
to the WRTO every time you order from Amazon online.  It’s easy! 
Instead of the usual website address, type in <a href=\"http://www.amazonsmile.com/\">www.amazonsmile.com</a> and follow the quick
and easy instructions.  You don’t have to
pay an extra penny, and 0.5% of your order automatically goes to the WRTO.  The only glitch is that we are trying to
change the address from Los Angeles (my address) to Boscobel, and are
struggling with that….but still gets donated to WRTO.<o:p></o:p>"); ?></p><p id="1431956002"><?php print _("Immaculate
Conception Church in Boscobel is having a Bless the Bikes (and roller-skates,
skateboards, etc.) on May 24th after 9:30 mass.  WRTO will have a table to share our bike trail
news and will also have tickets for the July 3rd Rooster Andy’s
Chicken BBQ, and tickets for other events also. 
Please bring your wheeled item for its/your blessing for a safe and fun
summer."); ?></p><p id="1431956002"><?php print _("June
6th from 9-noon at the Boscobel Fire Station, Boscobel Fire
Department, Gunderson Boscobel Hospital and Clinics, Wisconsin River Outings,
and WRTO will be holding a FREE bike safety class with a certified bike
trainer, for our kids.  Please register
your kids by calling 507-269-2606 or 608-391-0113, or emailing <a href=\"mailto:denisefisher5995@gmail.com\">denisefisher5995@gmail.com</a> or <a href=\"mailto:tmvial@yahoo.com\">tmvial@yahoo.com</a> for your
permission/registration slip. All
the kids need to do is bring their
bikes and their helmets (if they have one). 
We supply a fun lunch consisting of hot dogs, chips, granola bars,
treats, and a drink.  There will be fun
door prizes, and a grand prize drawing for a free canoe rental!  This is our chance to give back to our kids
and supporters.  We so look forward to
spending the morning with our community’s great kids!"); ?></p><p id="1431956002"><?php print _("There’s
a BIG GREEN (or red or blue…) tractor ride going on in Wauzeka on June 20th
starting at 9 am.  Bring your tractor and
ride to Steuben and back, eat a great lunch at Scott’s Shelter on Front Street
between 11-1, and continue on that afternoon into the country sides!  Lunch sales and raffle proceeds will be
donated to the WRTO, so come hungry!"); ?></p><p id="1431956002"><?php print _("Contact
any board member to get your ticket for the Rooster Andy’s Chicken BBQ this
July 3rd at Kronshage Park in Boscobel from 5-8 for $10.00.  The meal includes ½ BBQ chicken, potato
salad, beans, roll, drink, and dessert. 
Also on sale will be Schwan’s ice cream! 
We’ll have a fun DJ and face painting for the kids, as well as other
raffles and merchandise.  "); ?></p><p id="1431956002"><?php print _("Come
join us on our party bus to the Brewers game which will be followed by a Goo
Goo Dolls concert on August 15th. 
For $82/person, you get in to both events, a bus ride with Boscobel’s
famous Steve Dilley as our chauffeur, and fun raffles and games on the
bus!  We can’t wait!  The bus leaves at noon from Pat and Greg’s
Pour House in Boscobel, but we will make stops in towns along the way if you
want to jump in with us.  Call Denise
Fisher at 507-269-2606 or email at <a href=\"mailto:denisefisher5995@gmail.com\">denisefisher5995@gmail.com</a>
to reserve your ticket!  Here’s to a
Boscobel tailgating party!  WOOT! WOOT!"); ?></p><p id="1431956002"><?php print _("After
a 29 hour, straight-through drive, we made it back to Boscobel with the dog
(with her tongue out!).  It’s great to be
home and lay eyes on all the people we love!  "); ?></p><p id="1431956002"><?php print _("Have
a great week everyone!"); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Mactire McMullen</span>
        <span class="last-modified-date">Mon, May 18 15 08:33 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/wrt-news--may-18-2015" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>