<?php 
$rootPrefix="../";
$pageUrl="post/meeting-minutes-may-28-20";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="538ff2932353d";
$pageFriendlyId="meeting-minutes-may-28-20";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Meeting Minutes May 28, 2014</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="538ff2932353d" data-pagefriendlyid="meeting-minutes-may-28-20" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="meeting-minutes-may-28-20">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1401942683"><?php print _("Meeting Minutes May 28, 2014"); ?></h1><p id="p-1401942683"><?php print _("Bylaws and articles of association have been mailed to Boscobel for signatures and finalization.  Our 501(c)(3) status was discussed.  We have two years to complete the process.  Afterward, Dean Fisher stated he would finish our status for us!     "); ?></p><p id="1401943052"><?php print _(" Our web site is up and running, www.wirivertrail.org.  We are looking for volunteers to add new agenda items such as news or events into our site.  We will ask Mac McMullen if he is still interested. Our new logo will go on our web site soon."); ?></p><p id="1401943052"><?php print _("Roddy Dull finished our logo with great success.  He is going to price/create our shirts and possibly make logos for water bottles for future merchandise purchasing. "); ?></p><p id="1401943052"><?php print _(" Chris Stauffer in LaCrosse is working on our brochure to hand out over the 4th of July parade.  There will be a donation section in the brochure. "); ?></p><p id="1401943052"><?php print _("Prairie du Chien’s Courier Press and Fennimore’s Times newspaper have agreed to run a free column like the Dial does every week to keep people informed of our progress and news. "); ?></p><p id="1401943052"><?php print _("Jo Sommers continues to have the Community First Bank marquee in use for us whenever she can.  She stated our balance at our bank also. "); ?></p><p id="1401943052"><?php print _("Joel Leonard stated he could pick up our bike at Blue Dog Cycle in Viroqua for raffling.  We are still awaiting our raffle license from the State of Wisconsin. "); ?></p><p id="1401943052"><?php print _("Jeff and Mary Lee are going to be going around to businesses soliciting for their support and donations.  They have a note explaining our purpose and also a donation form. "); ?></p><p id="1401943052"><?php print _("Tonia sent a check to Rooster Andy’s for our July 3rd chicken BBQ party at the west shelter at Kronshage Park from 5-9 pm.  Food will be served from 5-7 pm.  The cost of the tickets will be $10 and need to be purchased ahead of time if possible.  We are shooting for 400 people!  There will be face painting, a DJ, and Schwan’s ice cream treats too (Schwan’s will be an extra purchase).  The meal consists of ½ BBQ chicken, potato salad, beans, roll, drink, and a dessert.   We have a sign up sheet for volunteers as well as asking people to bring water bottle and/or dessert donations for the evening.      "); ?></p><p id="1401943052"><?php print _("The A &amp; W Restaurant is helping us fundraise on Wednsday, June 4th from 4-8 pm.  Twenty percent of their profits will be donated to our Trail.  There will be door prizes available as well as volunteers and board members available to talk to that evening.  We will be selling shirts (ordering) with our new logo on them.  Wendi is going to buy the door prizes so we have a give away at least every ½ hour of our evening."); ?></p><p id="1401943052"><?php print _("Tonia is checking with Dairy Queen to see if they would like to match a night like A &amp; W has.  She is also seeing if China King would allow us to deliver like Krachey’s has done.  We talked about possibly creating a rotating schedule of food delivery within the town so people could count on deliveries on certain nights. "); ?></p><p id="1401943052"><?php print _("Joel is going to attend a meeting of the Boscobel Community Foundation at 10:30 at Community First Bank on Thursday, May 29th, to inform them of our goals and to ask for donations or price matching any of our fundraisers. "); ?></p><p id="1401943052"><?php print _("Denise is going to buy/order bracelets for sale for the 4th of July. "); ?></p><p id="1401943052"><?php print _("Jake Bacon is almost done with the big bike.  He is going to take the old bikes that we bought with Roddy Dull from the police department and spray paint them for our “yard decorating”.  Jake is going to contact Roddy Dull in regards to placing a sign on the big bike for advertising.  We owe Roddy Dull $100 for the bike purchases yet. "); ?></p><p id="1401943052"><?php print _("Bar raffles will begin this fall as a project. "); ?></p><p id="1401943052"><?php print _("Denise is going to contact Kwik Trip to be their first fundraiser when they open their doors.  "); ?></p><p id="1401943052"><?php print _(" Discussion about our November 1st Hiking and Biking Unlimited began.  We want to make this our huge fundraiser.  It is going to be at the Boscobel Bowl and Banquet.  Thoughts and ideas of donations/hand made crafts were discussed as well as entertainment and silent auctioning.  We discussed having a late summer/early fall event similar to the Cannonball Run that the bars have to support part of our fundraiser.  Denise is going to contact Frank Fiorenza from Potosi, as he said this is the Potosi Brewery’s biggest fundraiser so we do this up right and have lots of fun at it.  "); ?></p><p id="1401943052"><?php print _("Brat feeds were discussed but we discussed doing it in later summer also. "); ?></p><p id="1401943052"><?php print _(" I’m positive I’ve forgotten something so please let me know what I’ve missed!  Thanks for your interest!  Please feel free to email me or any board member with thoughts, questions, or ideas. Thanks, Denise"); ?></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Thu, Jun 05 14 06:37 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/meeting-minutes-may-28-20" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>