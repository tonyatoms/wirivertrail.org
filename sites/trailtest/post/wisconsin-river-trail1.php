<?php 
$rootPrefix="../";
$pageUrl="post/wisconsin-river-trail1";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="544865ba0e9d9";
$pageFriendlyId="wisconsin-river-trail1";
$pageTypeUniqId="5339d0edb7056";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - WISCONSIN RIVER TRAIL</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="544865ba0e9d9" data-pagefriendlyid="wisconsin-river-trail1" data-pagetypeuniqid="5339d0edb7056" data-api="http://atoms.net/wirivertrail-dev" id="wisconsin-river-trail1">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=534570596627036";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /#fb-root (comments support) -->	

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1414030787"><?php print _("WISCONSIN RIVER TRAIL"); ?></h1><p id="p-1414030787"><?php print _("OCTOBER 19, 2014"); ?></p><p id="1414031089"><?php print _("All I can think about as I write this is how proud I am of our hometown and area people.  The support for our banquet has been nothing short of amazing!  And it’s not about the amount, it’s about giving what you can, believing in leaving our community a little bit better off one day, and saying we did this together!"); ?></p><p id="1414031089"><?php print _("Our ticket sales for the banquet have been great, but the good news is that we do have some tickets left.  If you would like a ticket, please stop in at any of our Boscobel Banks, or call, text, or FB Denise Fisher at 507-269-2606 for delivery, or for an updated list of our donations and prizes.  Once you get a look at what we have available, you’ll for sure want to come.  For $25/person, everyone will go home full from a delicious meal, and have the opportunity to bid on or win various prizes. At least ½ of the attendees will come away with free gifts/gift certificates.  If you buy your ticket before October 24th, you will qualify for 5 extra tickets for some great items.  Joe Thomas from the Cleveland Browns owns land locally, and donated $1000 to the banquet/trail project!!  Here is a short list of our bigger items to date: 4 Badger men’s basketball tickets, 4 Badger men’s hockey tickets, 30-06 Savage camo gun with scope, Coach purse, Wisconsin Dells weekend, stand-alone tree stand, massages, Potosi weekend, Joe Trumm, Chad Fitzgerald, Dave Bennett, Shorty Youngs, Dave Gardner, and Russ Beck’s woodwork,  Carole Young’s painting of the bridge and another of the river/Shockley hill, Ipad, Ipod, laptop computer, kindle, Verizon cell phones, Packer memorabilia, Wii with games, Fiskars yard work tools X 13, tools gift certificate/tool container, bistro set, snowmobile jacket, meat, golfing package for 4, grills, quilts, tickets to a dinner theatre and Milwaukee zoo, Santa Claus visit, $500 wildlife print, jogging stroller, wine baskets, free housecleaning, day of guiding, local photography prints/packages, dumpster, firewood, Bulldog wares, 40” smart TV, Trek 7000 bike, Cannondale silk trail bike, and Boomer bike with electric assist.   FOR REAL!!!  We haven’t even bought at Cabela’s yet, but will be on our way there next week…."); ?></p><p id="1414031089"><?php print _("Speaking of Cabela’s….Their representative has encouraged us to apply for a grant/sponsorship.  Last year, they donated $90,000 to a local project and are looking for another worthy, outdoor project."); ?></p><p id="1414031089"><?php print _("Listen to our local radio stations within the next 2 weeks.  WPRE, WRCO, and WVRQ are doing a story on us.  We have also been asked to present at the Morning Show with Christine Bellport and Charlie Shortino within the next couple of weeks.  How exciting is THAT?!?!  We will keep you posted on the airing days via our facebook group page, Wisconsin River Trail Project."); ?></p><p id="1414031089"><?php print _("I hope you feel the energy. We’re building a trail!  Thanks so much for all your support, donations, and words of encouragement.  Hope to see you all at the banquet on November 1st!"); ?></p><p id="1414031089"><?php print _("Denise Fisher"); ?></p><h2 id="wisconsin-river-trail1-h2-10"><?php print _("Thanks for the Following Donations!"); ?></h2><p id="1414188785"><?php print _("Thanks for the Following Donations!"); ?></p><ul id="wisconsin-river-trail1-ul-12"><li><?php print _("A &amp; W-4-$5 gift cards"); ?></li><li><?php print _(" Achenbach Printing-$20 donation "); ?></li><li><?php print _("Alterations by Susan- "); ?></li><li><?php print _("American Family Insurance-$100 donation"); ?></li><li><?php print _("American Signs and Designs"); ?></li><li><?php print _("Heather Atkinson-Puckett-Nail wraps"); ?></li><li><?php print _("Russ Beck-Aldo Leopold bench"); ?></li><li><?php print _("Barb and Tom Bell-1 hour massage at Healing Hands"); ?></li><li><?php print _("Dave Bennett-wooden craft"); ?></li><li><?php print _("Blaine Theatre-Theatre basket-tickets, popcorn, treat, and pop"); ?></li><li><?php print _("Blue Dog Cycle-$65 gift certificate for a bike tune-up and bike socks"); ?></li><li><?php print _("Boscobel Auto Clinic-Bulldog carrying tote"); ?></li><li><?php print _("Bob’s Auto Body-Aaron Rodgers print w/attached 4 trading cards"); ?></li><li><?php print _("Boscobel Auto Body-$25 donation"); ?></li><li><?php print _("Boscobel Bowl and Banquet-Bowling, shoes, pitcher of pop, and pizza for 4 X 2"); ?></li><li><?php print _("Boscobel Dial-Dial Subscription "); ?></li><li><?php print _("Boscobel Hotel-Drink tokens "); ?></li><li><?php print _("Boscobel Lions Club-$25 donation "); ?></li><li><?php print _("Boscobel Lumber Company-$300 donation "); ?></li><li><?php print _("Boscobel Pharmacy-$25 donation "); ?></li><li><?php print _("Boscobel Swimming Pool-Family Swimming Pool pass for 2015 "); ?></li><li><?php print _("John and Pam Bunts-Santa Claus visit ($50 value)"); ?></li><li><?php print _("Carolyn’s Creations-$5 off haircut/$10 off perm or color"); ?></li><li><?php print _("Cenex/New Horizon-$25. Gas card"); ?></li><li><?php print _("Century 21-$100 Donation"); ?></li><li><?php print _("Century Link-$500 wildlife print"); ?></li><li><?php print _("Century Link-8 Verizon cell phones, 3 cordless house phones"); ?></li><li><?php print _("Chanhassen Dinner Theatre-Twin Cities, MN-2 free dinner and play-value $175."); ?></li><li><?php print _("Clare Bank-2X$25 Bosco Bucks"); ?></li><li><?php print _("Dr. Larry Clark, DDS-$100 donation"); ?></li><li><?php print _("Classic Reruns-$20 shopping gift certificate"); ?></li><li><?php print _("Community First Bank-$100 donation"); ?></li><li><?php print _("Companion Care Veterinary Clinic-"); ?></li><li><?php print _("Cornell Plumbing-$100 donation"); ?></li><li><?php print _("Cost Cutters, Jenny Strand-hair care products"); ?></li><li><?php print _("DA Diggers-"); ?></li><li><?php print _("Dairy Queen - Box of Dilly Bars"); ?></li><li><?php print _("Dan Hackett Photography-canvas-framed outdoor photo"); ?></li><li><?php print _("Paul and Paula Davis-Gardening  package"); ?></li><li><?php print _("Deb’s Flowers-Welcome door stop"); ?></li><li><?php print _("Dependable Solutions-"); ?></li><li><?php print _("Steve Dilley-load of firewood"); ?></li><li><?php print _("Dollar General-"); ?></li><li><?php print _("Double K’s-Gas Grill"); ?></li><li><?php print _("Dubuque Fighting Saints Hockey-Autographed hockey pucks X 2, autographed team photo, hat"); ?></li><li><?php print _("Amanda Duran-Origami owl"); ?></li><li><?php print _("Elliott Builders-2-$25 gas cards and outdoor hunting chair w/cooler attached"); ?></li><li><?php print _("ERA Realty, Sally Waltz-Gatherings gift "); ?></li><li><?php print _("Fillback Ford-"); ?></li><li><?php print _("Frazier Flooring-"); ?></li><li><?php print _("Dean and Denise Fisher-Savage 30-06 camouflage gun with scope"); ?></li><li><?php print _("Dean and Denise Fisher-Xmas Story basket-Red Ryder BB gun, Xmas Story DVD, Leg lamp"); ?></li><li><?php print _("Randy Galer-$25 donation"); ?></li><li><?php print _("Dave and Sue Gardner-decorative shelving ladder"); ?></li><li><?php print _("Gasser’s True Value-Bistro set (sm.table/2 chairs)"); ?></li><li><?php print _("The Gatherings"); ?></li><li><?php print _("Gently Down the Stream Acupuncture-Gift certificate for acupuncture treatment"); ?></li><li><?php print _("Glasbrenner Insurance-$50 donation"); ?></li><li><?php print _("Green Bay Packers-"); ?></li><li><?php print _("Green River Repair-$20 donation"); ?></li><li><?php print _("Eric Griswold, Minnesota-$25 donation"); ?></li><li><?php print _("H &amp; N-collectible toy truck"); ?></li><li><?php print _("Hair of the Dog-Doggie basket"); ?></li><li><?php print _("Healing Hands Massage-Hot Stone Massage"); ?></li><li><?php print _("Hickory Grove Cabinetry, Chad Fitzgerald-"); ?></li><li><?php print _("Hickory Grove Golf Course-9 holes with cart for 4 people"); ?></li><li><?php print _("Hildebrand Memorial Library-10-$5.00 gift certificates for book sale or fine forgiveness"); ?></li><li><?php print _("Hillside Greenhouse-$25 gift certificate"); ?></li><li><?php print _("Jesse and Brenda Kreul-4 Badger basketball tickets, 4 Badger Hockey tickets"); ?></li><li><?php print _("Tim and Lisa Jacobson-Mysteries of the Driftless DVD"); ?></li><li><?php print _("Diane and Gary Johnson-Bird feeder basket"); ?></li><li><?php print _("Donna and Doug Johnson-red neck cooler"); ?></li><li><?php print _("J &amp; J Sewing-$150 donation"); ?></li><li><?php print _("Ben Johnston-matted water color painting"); ?></li><li><?php print _("Jon &amp; Jen’s Tall Tail Sports and Spirits-fixed tree stand"); ?></li><li><?php print _("KB Builders-"); ?></li><li><?php print _("Kendall Funeral Homes-"); ?></li><li><?php print _("Kim’s Silver Dollar-case of beer"); ?></li><li><?php print _("Krogen’s Do It Best Store-Tool carrying case with $50. Gift card"); ?></li><li><?php print _("Marianne and Karl Krogen-wine gift basket"); ?></li><li><?php print _("Kwik Trip-pizza and gas gift cards/certificates"); ?></li><li><?php print _("Mary and Jeff Lee-Wine Spa Basket"); ?></li><li><?php print _("Joel and Karen Leonard-5 day stay at cabin in Northern Wisconsin!!"); ?></li><li><?php print _("Joel and Karen Leonard-Canned Goods Basket"); ?></li><li><?php print _("Joel and Karen Leonard-3 boxes of 22 shells (50 count each) "); ?></li><li><?php print _("Lisa’s Tax Solutions - Avon gift basket"); ?></li><li><?php print _("Northwood Retreat/Llaughing Llama Farms-1 night cabin stay at Grandma’s cabin (room for up to 6)"); ?></li><li><?php print _("Lonnie and Millie’s Hair Salon-14” charcoal grill"); ?></li><li><?php print _("Madison Mallards-Millie Bobble Head"); ?></li><li><?php print _("Robin and Jack McLimans-Beef"); ?></li><li><?php print _("Mac McMullen-Schwinn 1995 mountain bike, never ridden!"); ?></li><li><?php print _("Kayte McQuillan-quilt"); ?></li><li><?php print _("Midwest Silo-$25 donation"); ?></li><li><?php print _("Miller Pad &amp; Paper-Art/writing supplies"); ?></li><li><?php print _("Milwaukee Zoo-2 tickets"); ?></li><li><?php print _("Misty Spray Car Wash-10 car wash tokens"); ?></li><li><?php print _("Orange Leaf Frozen yogurt-$20 family fun pack X 2"); ?></li><li><?php print _("Randy and Lori Moret-Beef"); ?></li><li><?php print _("Lori Moret and Denise Fisher-house cleaning"); ?></li><li><?php print _("Marsha Parker-afghan"); ?></li><li><?php print _("People’s State Bank-$25 donation"); ?></li><li><?php print _("Doc and Madge Pelz-tricycle and Trek Bicycle"); ?></li><li><?php print _("Holly Pendleton-"); ?></li><li><?php print _("Greg Plotz-$200 donation"); ?></li><li><?php print _("Potosi Brewery"); ?></li><li><?php print _("Pour House-410 shot gun"); ?></li><li><?php print _("Ryan and Glenda Reynolds-"); ?></li><li><?php print _("Mr. Tom’s-Clay Matthews wall hanging"); ?></li><li><?php print _("River Inn Motel-9 hours of free swimming"); ?></li><li><?php print _("River Valley Power Sports-Snowmobile coat"); ?></li><li><?php print _("Lisa and Kevin Roth-Bob’s Bitchin’ BBQ sauce basket"); ?></li><li><?php print _("Scenic Valley Winery, Lanesboro, MN-$15 gift card"); ?></li><li><?php print _("Shear Heaven Hair Salon, Soldier’s Grove- Women’s salon package (shampoo, conditioner, hairspray-men’s salon package"); ?></li><li><?php print _("Silent Woman - $25 gift certificate"); ?></li><li><?php print _("Jo and Craig Sommers - $100 lottery ticket tree"); ?></li><li><?php print _("John Stagman-autographed book “The Machine Chronicle’s Emergence”"); ?></li><li><?php print _("Steve Stahlman-Day of Guiding (hunting or fishing)"); ?></li><li><?php print _("Jen Strand-"); ?></li><li><?php print _("Studio Noveau, Gays Mills-free sitting fee for family portrait/pictures"); ?></li><li><?php print _("Subway-6 gift certificates for 6” subs"); ?></li><li><?php print _("Superior Trophies-"); ?></li><li><?php print _("Tastefully Simple-Heather Copus-Gift basket"); ?></li><li><?php print _("Tax Works-2X$10 tax preparations"); ?></li><li><?php print _("Thermogas-gas grill cylinder fills"); ?></li><li><?php print _("Joe Thomas, Cleveland Browns - $1000 donation"); ?></li><li><?php print _("Timber Lane Coffee Shop-Coffee Gift Basket"); ?></li><li><?php print _("Trendsetters-Beauty basket-Redken shampoo, cond., hair spray, sculpting cream, lipstick, fingernail/cuticle conditioner"); ?></li><li><?php print _("Joe Trumm-wooden handcrafted item"); ?></li><li><?php print _("Unique Café-4-$25 gift certificates"); ?></li><li><?php print _("UW-Badger autographed, framed football photo"); ?></li><li><?php print _("Tammy and Dean Vial-$25 donation"); ?></li><li><?php print _("Taxworks-$10 tax preparation X 2"); ?></li><li><?php print _("Town &amp; Country Sanitation-Large dumpster"); ?></li><li><?php print _("Darcy K. Trumm Photography-photo"); ?></li><li><?php print _("Fred Waltz-$100 donation"); ?></li><li><?php print _("Weggy Winery-$25. Gift certificate"); ?></li><li><?php print _("Joe Welsh-load of firewood"); ?></li><li><?php print _("Westridge Farms/Organic Valley-Vegetables basket"); ?></li><li><?php print _("Violet Workman-Baby quilt"); ?></li><li><?php print _("Wisconsin River Outings-$200 donation"); ?></li><li><?php print _("World of Variety -1 Bulldog jacket, 1 Bulldog hooded sweatshirt"); ?></li><li><?php print _("Williams Chiropractic -Biofreeze ointment X 2 and $25 donation "); ?></li><li><?php print _("Carole Young-framed and matted art print of Wisconsin River"); ?></li><li><?php print _("Brian (Shorty) Youngs-wood carvings X 4"); ?></li><li><?php print _("Lindy Zart-2 autographed books (author)"); ?></li><li><?php print _("Derek and Casey Zimpel-15# hamburger"); ?></li><li><?php print _("3M-2 boxes full of 3m products"); ?></li></ul><h2 id="wisconsin-river-trail1-h2-13"><?php print _("The following Items will be bought with donation funds!"); ?></h2><ul id="ul-13"><li><?php print _("40” flat screen 1080 TV ($400 value)"); ?></li><li><?php print _("iPod ($192 value)"); ?></li><li><?php print _("Ipad "); ?></li><li><?php print _("Kindle"); ?></li><li><?php print _("Laptop computer"); ?></li><li><?php print _("Wii with games ($315 value)"); ?></li><li><?php print _("Running/Jogging Stroller ($140 value)"); ?></li><li><?php print _("Coach Purse ($315 value)"); ?></li><li><?php print _("Green Bay packer decorative light"); ?></li><li><?php print _("Wisconsin Badgers decorative light"); ?></li></ul><p id="1414188785"><?php print _("More ideas coming in….please send me gifts we should purchase…"); ?></p><p id="1414188785"><?php print _("Working on..."); ?></p><ul id="wisconsin-river-trail1-ul-13"><li><?php print _("Weekend in Potosi-$10 gift certificate to Wanda’s Café, Y’Allbee Tubing, one night at the Potosi Inn ($70) "); ?></li><li><?php print _("Family vacation package to Wisconsin Dells-Timbavati Wildlife Park tickets X4, gift certificate at Cheesecake Heaven Bakery"); ?></li><li><?php print _("Wisconsin River Trail Organization-numerous t-shirts, water bottles"); ?></li><li><?php print _("One week free rental of “Biking” someone’s yard"); ?></li></ul><p id="1414188785"></p><p id="1414188785"></p></div></div>
</div>

<div class="container" role="main">  
  <div class="blog-meta">
      <p>
        
        <?php print _("Last modified by"); ?>
        <span class="author">Tony Adams</span>
        <span class="last-modified-date">Sat, Oct 25 14 12:13 am</span>
      </p>
    </div> 
</div>
  
<div id="comments" class="container">
    <div id="block-1" class="block row">
      <div class="col col-md-12">
        <h3><?php print _("Comments"); ?></h3>
        <div class="fb-comments" data-href="http://wirivertrail.org/post/wisconsin-river-trail1" data-numposts="5"></div>
      </div>
  </div>
</div>   
 
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
            <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <!--<ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>-->
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>