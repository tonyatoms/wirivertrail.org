<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1432651569">WRT News - May 25, 2015</h1><p id="p-1432651569">Next
Wednesday, May 27th is our Second Annual board meeting and we need
your help.  We’ve accomplished so many
things this past year and look forward to all the fun events and forward
progress we will make next year.  We are
in need of more volunteerism and board members, and if you are interested in
seeing this trail become a reality (sooner rather than later), then please make
time to come to our meeting.  Many hands
make light work, and we always have fun! 
Come see what we’re all about…we promise that we will not expect more of
you than what you are willing to give. 
Stop by at 7:00 pm at 1404 Wisconsin Avenue in Boscobel.</p><p id="1432651838">The
winner of our early bird sign up for bike safety is Teagan Ward, child of Terra
and Travis Ward! He will receive a free
WRTO t-shirt.  June 6th from
9-noon at the Boscobel Fire Station, Boscobel Fire Department, Gunderson
Boscobel Hospital and Clinics, Wisconsin River Outings, and WRTO will be
holding a FREE bike safety class with a certified bike trainer, for our
kids.  Please register your kids by
calling 507-269-2606 or 608-391-0113, or emailing <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> or <a href="mailto:tmvial@yahoo.com">tmvial@yahoo.com</a> for your
permission/registration slip.  All the
kids need to do is bring their bikes and their helmets (if they have one).  We will be giving away
a limited number of new, free helmets for those kids that don’t have one,
courtesy of Gunderson Boscobel Hospital and the Children’s Miracle Network.   We supply a fun lunch consisting of hot dogs,
chips, granola bars, treats, and a drink. 
There will be fun door prizes, and a grand prize drawing for a free
canoe rental!  This is our chance to give
back to our kids and supporters.  We so
look forward to spending the morning with our community’s great kids!</p><p id="1432651838">Last Saturday’s farmers market saw many
more visitors and lots of fun.  Thanks to
all who stopped in, asked questions, or purchased tickets or merchandise.  The WRTO has a booth where we sell baked
goods, tickets to upcoming events (Rooster Andy’s), have fliers of what we’re
up to for fun, and are willing to talk “Trail Talk” with anyone
interested.  If you are able to donate
any baked goods, please call 507-269-2606. 
It was great to get to visit with so many familiar faces and meet new
people too.</p><p id="1432651838">You
can donate to the WRTO every time you order from Amazon online.  It’s easy! 
Instead of the usual website address, type in <a href="http://www.amazonsmile.com/">www.amazonsmile.com</a> and follow the quick
and easy instructions.  You don’t have to
pay an extra penny, and 0.5% of your order automatically goes to the WRTO.  The only glitch is that we are trying to
change the address from Los Angeles (my address) to Boscobel, and are
struggling with that….but still gets donated to WRTO.</p><p id="1432651838">There’s a BIG GREEN (or red or blue…) tractor ride going on in Wauzeka
on June 20th starting at 9 am. 
Bring your tractor and ride to Steuben and back, eat a great lunch at
Scott’s Shelter on Front Street between 11-1, and continue on that afternoon
into the country sides!  Lunch sales and
raffle proceeds will be donated to the WRTO, so come hungry!  Volunteers are needed to help serve at our
food stand.<o:p></o:p></p><p id="1432651838">Contact
any board member to get your ticket for the Rooster Andy’s Chicken BBQ this
July 3rd at Kronshage Park in Boscobel from 5-8 for $10.00.  The meal includes ½ BBQ chicken, potato
salad, beans, roll, drink, and dessert. 
Also on sale will be Schwan’s ice cream! 
We’ll have a fun DJ and face painting for the kids, as well as other
raffles and merchandise.  We are trying
to top last year’s amazing turnout of 500 people.  Tickets are sold at Krachey’s, Casey’s, the
Dial, and all the banks in Boscobel.  We
can’t wait to taste the delicious chicken….and yes, we DO deliver!</p><p id="1432651838">Come
join us on our party bus to the Brewers game which will be followed by a Goo
Goo Dolls concert on August 15th. 
For $82/person, you get in to both events, a bus ride with Boscobel’s
famous Steve Dilley as our chauffeur, and fun raffles and games on the
bus!  We can’t wait!  The bus leaves at noon from Pat and Greg’s
Pour House in Boscobel, but we will make stops in towns along the way if you
want to jump in with us.  Call Denise
Fisher at 507-269-2606 or email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>
to reserve your ticket!  Here’s to a
Boscobel tailgating party!  WOOT! WOOT!</p></div></div>