<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1435803951">WTO News - July 1, 2015</h1><p id="p-1435803951">Just
a couple of reminders for our readers! 
Don’t forget to buy your tickets NOW for the July 3rd Rooster
Andy’s Chicken BBQ dinner at the Kronshage Park in Boscobel from 5-8 pm.  We will also have deliveries and carry outs
available!  For $10 your meal will
consist of ½ chicken BBQ, potato salad, baked beans, roll, dessert, and a
drink!  We need to order the chicken
ahead of time from Rooster Andy’s to make sure everyone gets a delicious
meal!  Tickets are on sale at the Dial,
Boscobel Bowl &amp; Banquet, Trendsetters, all of our Boscobel banks and most
of the gas stations in Boscobel.  We also
take reservations for your meal and you can pay the evening of the BBQ
too.  Please email <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> or call
507-269-2606 to reserve your ticket!  We
are trying to sell 600 meals this year! 
You won’t be disappointed OR go home hungry!  We will have extra meals ordered and will
sell the dinners until we’re sold out!  </p><p id="1435804307">At
the chicken BBQ, we are raffling off $5 and $10 card raffles for a set of 2 and
a set of 4 Packer family fun days to be held on August 8.  There will be a card raffle on a brand new
1995 Schwinn BMX bike valued at $500 also! 
Our awesome merchandise including summer tanks, yoga pants, hoodies,
t-shirts and more will be on sale too. 
Did I mention Schwan’s ice cream too? 
Face Painting?  A DJ?  Don’t miss our fun family night!</p><p id="1435804307">We
will have a stand on July 4th at the Farmer’s market and will be
providing sausage and egg breakfast sandwiches, along with pulled pork
sandwiches and other fun items from 8-11:30. 
Please stop by and say hi to your WRTO volunteers!</p><p id="1435804307">And last but not least, if your child was a graduate of
the WRTO bike safety program and would like to join us in following the big
green bike in the parade with their bike and helmets, stop at the fire station
at 11:00 on the 4th of July for our parade number.  We have asked to be in the front of the
parade so the kids can enjoy the parade too.<o:p></o:p>

     Have a safe
and enjoyable 4th of July everyone!<o:p></o:p></p></div></div>