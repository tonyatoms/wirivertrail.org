<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1430795226">WRT News - May 3, 2015</h1><p id="p-1430795226">Our annual WRTO meeting will be held on Wednesday, May 27th
at 7 pm at 1404 Wisconsin Avenue.  We
would LOVE it if you could come and support our trail.  We will be voting in some new board members,
defining our next year’s goals, and, as always, enjoying each other’s
company!  We will need more help this
year as one of our members is accepting a new job, and we are going to be
working hard on making progress for our phase 2 towards Woodman, as well as
finalizing our funding and forward progress with phase 1.  If we don’t have the volunteerism, then our
time frame will be delayed.  PLEASE reach
out and help us complete the trail.  The
faster we get it done, the longer we have to use it in our lifetime….and enjoy
watching our children and grandchildren use it too!<o:p></o:p></p><p id="1430795569">This Friday we
are going to draw the winners of our Mother’s Day baskets at Madge’s Timber
Lane Coffee Shop. It’s not too late to get your chance to win!  $2/ticket or 6/$10.  One basket is a wine basket and the other is
a TLC Coffee basket.  Thanks to Madge and
Doc Pelz, and Marianne and Karl Krogen for donating these fun baskets!<o:p></o:p></p><p id="1430795569">June 6th
from 9-noon at the Boscobel Fire Station, Boscobel Fire Department, Gunderson
Boscobel Hospital and Clinics, Wisconsin River Outings, and WRTO will be
holding a FREE bike safety class with a certified bike trainer, for our
kids.  Please register your kids by calling
507-269-2606 or 608-391-0113, or emailing <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> or <a href="mailto:tmvial@yahoo.com">tmvial@yahoo.com</a> for your permission/registration
slip.  All the kids need to do is bring
their bikes and their helmets (if they have one).  We supply a fun lunch with hot dogs, chips,
granola bars, treats, and a drink.  There
will be fun door prizes, and a grand prize drawing for a free canoe rental!  This is our chance to give back to our kids
and supporters.  We so look forward to
spending the morning with our community’s great kids!<o:p></o:p></p><p id="1430795569">Don’t forget
our big green bike for graduations and/or birthdays!  It’s available for just $15.  If it lands in your yard, you can move it to
someone else’s yard for $10 or just remove it from your yard for $15.  Call Joel at 375-5708 to reserve!<o:p></o:p></p><p id="1430795569">The
Boscobel Farmer’s Market is less than ONE WEEK from becoming a reality!  Yeah! 
The WRTO is sponsoring a booth, and will sell baked goods, plants,
clothing, and merchandise, as well as sell tickets to the Rooster Andy’s 3rd
of July Chicken BBQ in Kronshage Park. 
Last year we sold 500 tickets and this year we hope to sell 600!  Please support our local area vendors and
stop in and say hi to us at our booth! 
If you would like to donate a baked good or plants, call or email
the numbers above!  We would love to have
your home baked goods to sell!<o:p></o:p></p><p id="1430795569">WHO’S COMING TO THE BREWER
GAME/GOO GOO DOLLS CONCERT WITH US?  We’re
so stinking excited to have some fun! 
For $82, you get to tailgate with a bunch of fun people, watch the
Brewer’s and then listen to the Goo Goo Dolls after the game.  Reserve your spot by calling Denise at
507-269-2606 or emailing.  We leave on
August 15 from the Pour House at 12:00 for a 6 pm game.  There will be prizes and games on the
bus…more fun than we should all have on one bus!  We will make stops if you want to jump in at
Fennimore or Dodgeville also!<o:p></o:p></p><p id="1430795569">Everyone have a great week!  Dean, Gabby (the dog), and I are on our way
home next Saturday!  Stop in and say hi!<o:p></o:p></p></div></div>