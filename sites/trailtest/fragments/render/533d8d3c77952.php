<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1396542786">Events</h1><p id="p-1396542786"></p><ul id="list1" class="respond-list list-group" data-bind="foreach: list1" 
	data-display="list" 
	data-label="events" 
	data-pagetypeid="533d8b445055d" 
	data-length="10" 
	data-desclength="250"
	data-orderby="Created" 
	data-category="-1">
        <li class="list-group-item">
        	<a class="pull-left thumbnail" data-bind="attr:{'href':url}, visible: hasImage">
	        	<img data-bind="attr: {'src': thumb}">
            </a>
            <h4><a data-bind="attr:{'href':url}, text:name"></a></h4>
			<small data-bind="visible: hasCallout, text: callout"></small>
			<p data-bind="text:desc"></p>
		</li>
</ul>

<p data-bind="visible: list1Loading()" class="list-loading"><i class="icon-spinner icon-spin"></i> Loading...</p>

</div></div>