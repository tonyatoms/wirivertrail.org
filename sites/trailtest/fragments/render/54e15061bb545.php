<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1424052328">WRT News - FEB 15,2015</h1><p id="p-1424052328">Progress abounds this week again! After lots and lots of paperwork, and a wonderful meeting
with Lisa’s Tax Solutions, we received our paperwork from the IRS stating that we are now officially a
501(c)(3) organization! This is retroactive to April 14, 2014, so any contribution that you have made or
plan on making are now tax deductible! WOOT! WOOT!</p><p id="1424052613">Our hearts are so full this week with a $200 donation that came to us from the Boscobel
Elementary School Student Council. How wonderful is that! Our children are supportive of our trail
which speaks loudly to all that if our kids see this as a wonderful thing, so should we. I would encourage
all to give just a little to our trail to watch it grow. On our website,<a href="http://www.wirivertrail.org/">www.wirivertrail.org</a>, there is a “Get
Involved” link and a way to donate every month, quarterly, or just once. You can donate as little as
$5.00/month, and you can easily arrange for your donation to come out of your checking/savings
account whenever you would like. If each of our households did this, we would be matching our DNR
grants as soon as they were received, which would put us digging in the ground by the end of next
summer. You can also drop your tax-deductible donation check off to Community First Bank at any
time. A HUGE THANK YOU goes out to our wonderful Boscobel Elementary School kids!</p><p id="1424052613">If you don’t have the ability to donate financially, the WRTO is more than happy to take your
volunteering donation. Our meetings are every 3rd Wednesday of the month, and we would love to
have your help in any way. Please contact a board member to donate any of your time or talents!
This last week we turned in a grant to the Natural Resources Foundation for almost $5000.00.
We will find out March 31st if we are the recipient of this grant. Keep your fingers crossed!
We will also find out within the next couple of weeks if we are the recipients of the Children’s
Miracle Network $500 grant that we applied for through Boscobel Gunderson Hospital and Clinics. This
grant would go towards fitting our kids with helmets prior to our bike safety class we are sponsoring on
June 6th at 9:00 a.m. Please mark your calendars!</p><p id="1424052613">Don’t forget to sign up for Kids and Canvas or Cocktails and Canvas on March 14th at
11:00 for the kids, or 4:00 for the adults at Double K’s. Cost is $40/person for the adults, and $32/child
to create a forever canvas painting. There are different paintings you can choose from. There are still
openings for each time slot. Call Studio Art Supply at 608-649-3000 to reserve your spot or for any
questions you may have on the event. All supplies are included! Please see our events page on our
Facebook website, Wisconsin River Trail Project. Come shake the winter blahs and inspire your inner
creative you!</p><p id="1424052613">Hope everyone had a wonderful Valentine’s Day! Have a great week!</p></div></div>