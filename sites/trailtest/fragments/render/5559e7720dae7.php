<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1431955319">WRT News - May 18, 2015</h1><p id="p-1431955319">Next Wednesday, May 27th is our Second Annual
board meeting!  We’ve accomplished so
many things this past year and look forward to all the fun events and forward
progress we will make next year.  We are
in need of more volunteerism and board members, and if you are interested in
seeing this trail become a reality (sooner rather than later), then please make
time to come to our meeting.  Many hands
make light work, and we always have fun! 
Come see what we’re all about…we promise that we will not expect more of
you than what you are willing to give. 
Stop by at 7:00 pm at 1404 Wisconsin Avenue in Boscobel.<o:p></o:p></p><p id="1431956002">Last
Saturday at the Boscobel Farmer’s Market may have been a little wet, but sure
was fun.  The WRTO has a booth where we
sell baked goods, tickets to upcoming events, have fliers of what we’re up to
for fun, and are willing to talk “Trail Talk” with anyone interested.  If you are able to donate any baked goods,
please call 507-269-2606.  It was great
to get to visit with so many familiar faces and meet new people too.</p><p id="1431956002"> Is anyone
interested in forming a women’s bike club where we could meet at a designated
spot daily?  It would be more of a
beginners group and could start a fun new exercise program for all involved.  Please call Denise at 507-269-2606 if you are
interested in meeting for a ride!<o:p></o:p></p><p id="1431956002">     You can donate
to the WRTO every time you order from Amazon online.  It’s easy! 
Instead of the usual website address, type in <a href="http://www.amazonsmile.com/">www.amazonsmile.com</a> and follow the quick
and easy instructions.  You don’t have to
pay an extra penny, and 0.5% of your order automatically goes to the WRTO.  The only glitch is that we are trying to
change the address from Los Angeles (my address) to Boscobel, and are
struggling with that….but still gets donated to WRTO.<o:p></o:p></p><p id="1431956002">Immaculate
Conception Church in Boscobel is having a Bless the Bikes (and roller-skates,
skateboards, etc.) on May 24th after 9:30 mass.  WRTO will have a table to share our bike trail
news and will also have tickets for the July 3rd Rooster Andy’s
Chicken BBQ, and tickets for other events also. 
Please bring your wheeled item for its/your blessing for a safe and fun
summer.</p><p id="1431956002">June
6th from 9-noon at the Boscobel Fire Station, Boscobel Fire
Department, Gunderson Boscobel Hospital and Clinics, Wisconsin River Outings,
and WRTO will be holding a FREE bike safety class with a certified bike
trainer, for our kids.  Please register
your kids by calling 507-269-2606 or 608-391-0113, or emailing <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> or <a href="mailto:tmvial@yahoo.com">tmvial@yahoo.com</a> for your
permission/registration slip. All
the kids need to do is bring their
bikes and their helmets (if they have one). 
We supply a fun lunch consisting of hot dogs, chips, granola bars,
treats, and a drink.  There will be fun
door prizes, and a grand prize drawing for a free canoe rental!  This is our chance to give back to our kids
and supporters.  We so look forward to
spending the morning with our community’s great kids!</p><p id="1431956002">There’s
a BIG GREEN (or red or blue…) tractor ride going on in Wauzeka on June 20th
starting at 9 am.  Bring your tractor and
ride to Steuben and back, eat a great lunch at Scott’s Shelter on Front Street
between 11-1, and continue on that afternoon into the country sides!  Lunch sales and raffle proceeds will be
donated to the WRTO, so come hungry!</p><p id="1431956002">Contact
any board member to get your ticket for the Rooster Andy’s Chicken BBQ this
July 3rd at Kronshage Park in Boscobel from 5-8 for $10.00.  The meal includes ½ BBQ chicken, potato
salad, beans, roll, drink, and dessert. 
Also on sale will be Schwan’s ice cream! 
We’ll have a fun DJ and face painting for the kids, as well as other
raffles and merchandise.  </p><p id="1431956002">Come
join us on our party bus to the Brewers game which will be followed by a Goo
Goo Dolls concert on August 15th. 
For $82/person, you get in to both events, a bus ride with Boscobel’s
famous Steve Dilley as our chauffeur, and fun raffles and games on the
bus!  We can’t wait!  The bus leaves at noon from Pat and Greg’s
Pour House in Boscobel, but we will make stops in towns along the way if you
want to jump in with us.  Call Denise
Fisher at 507-269-2606 or email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>
to reserve your ticket!  Here’s to a
Boscobel tailgating party!  WOOT! WOOT!</p><p id="1431956002">After
a 29 hour, straight-through drive, we made it back to Boscobel with the dog
(with her tongue out!).  It’s great to be
home and lay eyes on all the people we love!  </p><p id="1431956002">Have
a great week everyone!</p></div></div>