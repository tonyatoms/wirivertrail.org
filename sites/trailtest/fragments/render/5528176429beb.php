<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1428690802">WRT News - April 10, 2015</h1><p id="p-1428690802">     World of Variety has a new display of Wisconsin River
Trail merchandise.  We are so excited to
branch out and offer more than just t-shirts! 
We have baseball caps, visors, key chains, and many more items for you
to choose from, with more on the way too! 
Thanks again to World of Variety!<o:p></o:p></p><p id="1428691111">     Madge’s Timber
Lane Coffee Shop is displaying a couple of awesome Mother’s Day baskets to be
drawn on the Friday before Mother’s Day. 
We have an amazing wine basket and also a coffee basket, courtesy of
Marianne Krogen and Madge.  Tickets are
$2/each or 6 for $10.  What a great
Mother’s Day gift!<o:p></o:p></p><p id="1428691111">     June 6th from 9-noon is our 2nd
Annual FREE Bike Safety Training sponsored by the Gunderson Boscobel Hospital
and Clinics, Children’s Miracle Network, Boscobel Fire Department, and
WRTO.  For registration forms, please
contact Tonia at 391-0113 or <a href="mailto:toniavial@yahoo.com">toniavial@yahoo.com</a>
or Denise at 507-269-2606 or <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>.  We will also provide snacks and many prize
drawings for the kids too</p><p id="1428691111">     The
grant is in!  Yeah!  Now to hold our breath until we find out the
end of August or beginning of September! 
Keep your fingers crossed!  And
not to let grass grow under our feet, another grant of $3800 was applied for
from Cabela’s this week   </p><p id="1428691111">     Hope everyone has a great week! </p><div id="imagecontainer1" class="o-image"><img id="image1" src="sites/trailtest/files/WRTO and World of Variety.jpg"></div></div></div>