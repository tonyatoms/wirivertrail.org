<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1435064144">WTO News - June 22, 2015</h1><p id="p-1435064144">     The winner of
our Father’s Day basket was Crystal Krachey! 
The best part of this news is that she won a Krachey’s pizza from BP
(and a 12 pack of pop, 12 pack of beer, and an awesome backpack cooler!).  This last weekend we had a fun crowd in
Wauzeka at the boat landing, driving their tractors around the countryside and
enjoying our lunch too.<o:p></o:p></p><p id="1435064364">      Well, the 3rd of July is coming
in quickly!  It’s important that we try
to PRE-SELL as many tickets as we can so we have enough chicken at our Rooster
Andy’s chicken BBQ.  Please try to buy
your ticket ahead of time at any of Boscobel’s banks, the Dial, Boscobel Bowl
and Banquet, Trendsetters, most of the Boscobel gas stations, or call us at
507-269-2606, 608-391-0113, or 608-375-5708. 
We can also RESERVE your tickets and you can pay at the door that
evening.  It’s $10 for ½ chicken, potato
salad, baked beans, roll, dessert, and drink. 
There will be Schwan’s ice cream available, face painting, a DJ, and
kids’ games too.  We are raffling a .22
Ruger gun and a 1995 brand new Schwinn bike. 
Dining hours are from 5-8 (deliveries starting at 4:30) and we promise
you won’t be disappointed.  Last year we
sold 500 tickets and this year we are trying to sell 600.  If you don’t get a chance to reserve a
ticket, we will
still accept you at the door as long as our chicken supply lasts!  J  Come
hungry for sure!</p><p id="1435064364">       We
have tickets for sale for our Brewers and Goo Goo Dolls concert on August 15th.  For $82/person, you get in to both events, a
bus ride with Boscobel’s famous Steve Dilley as our chauffeur, and fun raffles
and games on the bus!  We can’t
wait!  The bus leaves at noon from Pat
and Greg’s Pour House in Boscobel, but we will make stops in towns along the
way if you want to jump in with us.  Call
Denise Fisher at 507-269-2606 or email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> to
reserve your ticket!  Here’s to a
Boscobel tailgating party! <o:p></o:p></p><p id="1435064364">      The Farmer’s Market is going great….when it isn’t
raining!  We will be serving
breakfast/lunch on the 4th of July, as well as continuing to sell
clothing, merchandise, and tickets to the Brewers game.  Stop in and ask us questions.  We are proud of our accomplishments that you
will be seeing in the next few months. 
We’ll have a trail begun by fall!<o:p></o:p></p><p id="1435064364">     Don’t forget our big green bike rental at
$15/3 days or until it’s rented out next. 
Call Joel at 375-5708. Don’t forget when ordering from Amazon online
that you make the Wisconsin River Trail your free charity at <a href="http://www.smileamazon.com/">www.smileamazon.com</a></p><p id="1435064364">Thanks
for supporting our fun adventures here in our beautiful neck of the woods!  </p></div></div>