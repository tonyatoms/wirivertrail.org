<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1400537017">Donate!</h1><p id="p-1400537017">Thank you for visiting our donation page.   We have set up a way for you to donate either one time, or over time, in smaller increments, so as not to tax your family budget.  If you click on the form below, feel free to print it out and complete it.  You can donate $5 or $10/month, not tax your budget, and help us in our quest to build a beautiful nature trail in and around the heart of the lower Wisconsin River.</p><p id="1400680866">If you choose to donate over time or as a one-time-donation over $500, you will qualify to have your name/memorial placed in a recognition area on our trail.</p><p id="1400680866">Thank you for dreaming this dream with us.  We look forward to meeting you on our trail… with our families… and a huge smile and a friendly hello!</p>

<p class="file">
	<i class="fa fa-file-text-o"></i>
	<a href="files/AUTHORIZATION_AGREEMENT_FOR_DIRECT_PAYMENTS.pdf"><?php print _("Download AUTHORIZATION_AGREEMENT_FOR_DIRECT_PAYMENTS.pdf "); ?></a>
</p>

<p class="file">
	<i class="fa fa-file-text-o"></i>
	<a href="files/Wisconsin River Trails 2014 Banquet Donations.pdf"><?php print _("Download Wisconsin River Trails 2014 Banquet Donations.pdf"); ?></a>
</p></div></div>