<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1436234402">WTO News - July 6, 2015</h1><p id="p-1436234402">     A huge thank
you goes out to all that came out on Friday night to enjoy our chicken BBQ with
us.  We sold out all 606 meals!  We’re sorry for those that were turned away,
and hope you come back next year to join us at our picnic in the park.  We are still raffling the Rueger .22 gun. We
have just a few playing cards left and will let everyone know as soon as we
draw a winner!  We also have 6 tickets to
the Packer family fun night on August 8th that we are raffling.  And last but not least come join us for a
Brewers game/Goo Goo Dolls concert on August 15th.  We’ve requested our local and infamous Steve
Dilley to drive our bus around!  Remember
to come to the farmer’s market to buy a ticket to our raffles, buy any of our
merchandise, or come find out what we’re up to next!<o:p></o:p></p><p id="1436234459">     We’re
exhausted, we’re happy, and we were so glad to see such great support for our
trail!  Thanks again to all our fun
volunteers and to you for supporting our trail!<o:p></o:p></p><p id="1436234459">     Have a great
week everyone!<o:p></o:p></p></div></div>