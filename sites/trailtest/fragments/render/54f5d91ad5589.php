<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><p>
  We’ve got preliminary maps and plans! Let the fun begin! After receiving these plans, we are now able to share them with the DNR/DOT/City Staff/Army Corps of Engineers. We will be sending out information to area contractors and related companies or posting it in the newspaper, in hopes to receive bids from our local area talents. If you are a contracting company would like to help or find out if you can help, please contact us at  <a href="mailto:denisefisher5995@gmail.com"> denisefisher5995@gmail.com</a>.
</p><p id="1425398247">Kids and Canvas and Cocktails and Canvas are coming up quickly on Saturday, March 14th at
Double K’s, with the kids at 11:00 and the adults at 4:00. We still have spots available! Call Studio Art
Supply at 608-649-3000 to book your day of fun or to find out more information. Kids and Canvas will be
for kids over age 6 and will need an adult with them to help. The cost is $32/child. Cocktails and Canvas
will be $40/person. We will have drawings for free gifts as well. As winter continues to drag on, come
do something fun for a great cause! Make your reservation soon as spots are filling up!</p><p id="1425398247">Our next meeting is Wednesday, March 18th at 6:30 pm at Marianne and Karl Krogen’s house on
the SW corner of East Street and County Road MS. Everyone is always invited to our meetings. This
meeting is going over different fundraising ideas. If you can’t make it to the meeting but have some
great ideas, please let us know. We’re all about having fun with our community!
Have a great week everyone!</p></div></div>