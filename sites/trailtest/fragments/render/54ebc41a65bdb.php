<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1424737310">WRT News - FEB 22, 2015</h1><p id="p-1424737310">     Last Wednesday at our monthly meeting, we decided to make our next month’s meeting about
our fundraisers for the year. We plan on making our new list of this year’s fundraising events. To avoid
volunteer burnout, we want to make MANY of the fundraisers count as much as possible while making
sure that all events create fun for everyone. If you are interested in helping us with thoughts on our
fundraisers, please feel free to come to our meeting, even if it’s just to check us out and see what we’re
up to! Any ideas that you have are welcomed!</p><p id="1424737562">     Work continues towards applying for our Knowles Stewardship grant. We have asked many in
the community to write their letter of support that we are asked to supply for grant purposes. If you
would like to submit a letter of support for the trail to the boat landing, please feel free to email it to
denisefisher5995@gmail.com by March 15th
. </p><p id="1424737562">Jeremy Krachey is working hard at supplying us with different plan variations to creating the trail
to the boat landing. Once those are done, we hand them in to the DNR, DOT, and Army Corps of
Engineers to get all of the permitting accomplished.</p><p id="1424737562">     Don’t forget to sign up for Kids and Canvas for $32/child or Cocktails and Canvas for $40/each
on March 14th at 11:00 for the kids, or 4:00 for the adults at Double K’s, to create a forever canvas
painting. There are different paintings you can choose from. There are still openings for each time slot.
Call Studio Art Supply at 608-649-3000 to reserve your spot or for any questions you may have on the
event. All supplies are included! Please see our events page on our Facebook website, Wisconsin River
Trail Project. Spring is coming! Let’s stay busy with fun family and friend events to do together!</p><p id="1424737562">     Mark your calendar to come share a fun night with the Wisconsin River Trail peeps on March
18th at 7 pm at Karl and Marianne Krogen’s house at our meeting!      Have a great week everyone!  </p></div></div>