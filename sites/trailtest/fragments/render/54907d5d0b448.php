<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1418755467">WISCONSIN RIVER TRAIL-DEC. 15, 2014</h1><p id="p-1418755467">The
Wisconsin River Trail Organization held it’s monthly meeting last week.  We worked on many things that had been put on
the back burner.  Here are most of our
thoughts and items we have in the hopper! </p><p id="1418755832">Saturday,
June 6th, 2015 will be our bike safety date.  We will send home a sign up form with the
students of Boscobel Elementary School mid-May, so we can give a head’s up to
the parents.  We’ll also post it on our
website, <a href="http://www.wirivertrail.org/">www.wirivertrail.org</a>, and
Facebook page, Wisconsin River Trail Project. 
We had so much fun with the kids last year, we look forward to training
them to use their bike safety skills throughout the summer of 2015.</p><p id="1418755832">We are looking into doing fun
activities to break up the winter.  One
is a “Chardonnay and Canvas” date in late February or early March for grownups,
with a class earlier in the day for parent/child “Chocolate Milk and Canvas”.  Please contact any of the board members if
you are interested in these classes and would like more information sent to you
as we get closer to the date.</p><p id="1418755832">Rooster Andy’s is in the works
again for Friday, July 3rd. We have already reserved the park with
the city. We will complete carry outs from 4:30-5:30, prior to serving food at the
park, which will be from 5:30-8:30. 
There will be bonus drawings for those that attend the BBQ and also
complete the Firecracker Walk/Run the next morning. We plan to have raffles for
prizes (similar to the drawings at the Hiking and Biking Banquet in
November).    We have applied for a $2500 grant from Grant
County Tourism to advertise this event to surrounding areas.</p><p id="1418755832">Denise
talked to Sue Howe, representative from the Outdoor Recreation Alliance, who is
planning on connecting us with some trail building engineers by the first week
of January.  She suggested having our
area local business owners and city officials attend their summit on March 25th
in LaCrosse.  This summit plans on
discussing the impact of tourism on Boscobel, as well as having the mayor of
Duluth, MN, talk about how he reinvented Duluth as our nation’s #1 rated place
to live for outdoor activities.</p><p id="1418755832">We also spoke with Angie Wright,
who is instrumental in Platteville’s trail that they are finishing right
now.  She gave us great tips on applying
for grants successfully, as well as discussing what our next steps could
possibly be.  Angie discussed many
different fundraisers that Platteville has used to raise money for their trail
and for matching DNR grants.</p><p id="1418755832">Our trail members plan to do
deliveries for a few restaurants in town again this winter.  We hope to be doing this on the weekends if
that’s amenable to the restaurant managers/owners.  We will keep you posted as to where these
fundraisers will be and the dates that have been chosen.  It was so much fun to do this last year for
the board and volunteers! </p><p id="1418755832">Our website, <a href="http://www.wirivertrail.org/">www.wirivertrail.org</a>, was updated with
the mission statement, bylaws, and articles of association.  On the home page, it is accessible by
clicking on the 501(c)(3) on the bottom left. 
We also tried to update our list of volunteers.  If you were inadvertently missed, please let
us know!  Finally, we are <a name="_GoBack"></a>updating the list of donations by businesses and individuals
and hope to have that posted soon.</p><p id="1418755832">That’s our news in a
nutshell!  We hope you have a great
week! </p></div></div>