<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1433772250">WTO News - June 7, 2015</h1><p id="p-1433772250">     Thirty-one of
our local kids have their bike safety training complete for the summer!  We had a great turnout and a wonderful time
with the kids!  Helmets were distributed
to those kids that needed one, and were donated by Gunderson Boscobel Hospital,
Boscobel Hospital Partners, and Walmart in Prairie du Chien.  Many door prizes were given away.  Our grand prize winners of a free canoe
rental were Sidney McCormick and Ryan Vial. 
Thanks to the Boscobel firemen, nurses from the hospital, and other
volunteers for helping us create an event that will keep our kids safe this
summer!  All graduates of the program are
invited to bike with us at the 4th of July parade!<o:p></o:p></p><p id="1433772878">We
had a meeting at Boscobel City Hall with the DOT, DNR, city staff, and project
engineer this last week to discuss the final plans for construction to begin in
September.  The meeting was a great,
informative meeting and a huge step towards forward progress.</p><p id="1433772878">Please
pick up your tickets in advance at most of the Boscobel gas stations, all of
the Boscobel banks, Boscobel Bowl and Banquet, and the Boscobel Dial for our
Rooster Andy’s Chicken BBQ in the park on July 3rd.  Tickets will also be available June 20th
at the Tractor Ride event in Wauzeka.  We
are trying hard to sell 600 tickets to our picnic. We DO deliver locally!  It’s $10 for ½ chicken, potato salad, baked
beans, roll, dessert, and drink.  There
will be Schwan’s ice cream available, face painting, a DJ, and kids’ games
too.  Hours are from 5-8 and we promise
you won’t be disappointed.  </p><p id="1433772878">     June 20th,
bring your tractor and join area farmers in driving from Wauzeka to Steuben and
back…and then more countryside driving after lunch too.  Lunch will be provided by the WRTO at Scott’s
Landing from 11-1.  Contact Tonia Vial at
391-0113 for more information.<o:p></o:p></p><p id="1433772878">     We have
tickets for sale for our Brewers and Goo Goo Dolls concert on August 15th.  For $82/person, you get in to both events, a
bus ride with Boscobel’s famous Steve Dilley as our chauffeur, and fun raffles
and games on the bus!  We can’t
wait!  The bus leaves at noon from Pat
and Greg’s Pour House in Boscobel, but we will make stops in towns along the
way if you want to jump in with us.  Call
Denise Fisher at 507-269-2606 or email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> to
reserve your ticket!  Here’s to a
Boscobel tailgating party! <o:p></o:p></p><p id="1433772878">Any
and all of our tickets and information are available weekly at Boscobel’s
Farmer’s Market event from 8-noon by the Boscobel Depot.  Clothing/merchandise are also on sale there,
as well as at World of Variety….and yoga pants and tank tops have arrived! </p><p id="1433772878">     Our Father’s
Day Raffle basket consists of a $40 soft side cooler, a large pizza from
Krachey’s, a 12-pack of soda, and a 12-pack of beer of your choice.  Tickets are on sale at the Farmer’s Market
and, during the week, at Timber Lane Coffee Shop for $1/each or 6
tickets/$5.00.  <o:p></o:p></p><p id="1433772878">     Don’t forget
any Amazon order you place can donate, at no extra cost to you, 0.5% of your
order.  Go to <a href="http://www.smileamazon.com/">www.smileamazon.com</a> to pick your charity,
the Wisconsin River Trail!<o:p></o:p></p><p id="1433772878">The
big green bike doesn’t like to be idle! 
Call Joel at 375-5708 to rent it out for any event!  We do travel it to nearby towns for a small fee.</p><p id="1433772878">Hope
everyone has the best week!  </p></div></div>