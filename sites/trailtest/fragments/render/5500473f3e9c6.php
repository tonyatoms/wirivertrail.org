<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1426081594">WRT News - March 8, 2015</h1><p id="p-1426081594">Just a few days left to reserve your spot for Kids and Canvas or Cocktails and Canvas event at
Double K’s, this Saturday, March 14th. Kids 6 and over (with a parent for a little help) are painting a
forever picture. Kids cost is $32/child and starts at 11:00 am. All supplies are included! Cocktails and
Canvas begins at 4:00 pm, costs $40/person and has just a few spots left open. We’re having some fun
drawings during the paintings too! Please call Sue at Studio Art Supplies at 608-639-3000 to reserve
your spots or for any questions you may have. Please see our Facebook group and event page at
Wisconsin River Trail Project for more information. </p><p id="1426081703">Madge’s Timber Lane Coffee Shop is sponsoring an Easter basket right now until Good Friday.
For $1/ticket or 6/$5.00, put your name in for a fun Easter basket drawing. Stop in for an awesome cup
of coffee and check out the fun Easter gifts inside! We plan on drawing a winner on Good Friday!  </p><p id="1426081703">Thursday, March 19th we have a big meeting at Boscobel City Hall. DNR, DOT, Army Corp of
Engineers, city staff, and trail board members are meeting to finalize a plan for the trail. There are a fe w
variations of options to consider, and due to the standards and rules that we have to follow, getting the
group all together is a great “round table”. We are shooting for trail construction to start this fall. Not
bad for a group that just formed in April of last year! We would like to thank the city staff and city
council for all their help, guidance, and extra time and talent we’ve received from them! </p><p id="1426081703">Have a great weekend!</p></div></div>