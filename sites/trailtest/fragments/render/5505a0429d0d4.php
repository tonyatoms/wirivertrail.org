<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1426432071">WRT News - March 15, 2015</h1><p id="p-1426432071">Saturday turned out to be a great day for Kids and Canvas and Cocktails and Canvas. Fun was had by
all and great forever memories will be hung on many area walls for years to come. Thank you to Sue
Price from Studio Art Supply and all who came out to support the Wisconsin River Trail and enjoy each
other’s company too! </p><p id="1426432401">There’s an awesome Easter basket that is on display at Madge’s Timber Lane Coffee Shop in Boscobel.
Tickets for the basket are $1.00/each or 6/$5.00. The winner will be drawn on Good Friday. Mother’s
Day baskets are in the works for the month of April up to Mother’s Day. </p><p id="1426432401">Wisconsin River Trail T-shirts and merchandise are going to be seen for sale around different
businesses in Boscobel. We are working on creating more merchandise and would like to thank all area
businesses for allowing us to sell our items in their stores.</p><p id="1426432401"> Cabela’s Outfitters has been in contact with us with news that we are in the running for a monetary
donation. More news to follow as we hear more on this gift! </p><p id="1426432401">Gunderson Boscobel Hospital and the Boscobel Fire Department are joining with the WRTO on June 6 th for a bike safety class. Sign-up sheets are circulating in the area and will go home with Boscobel Elementary School students. If you would like to sign your child up, or would like more information, please call Tonia at 391-0113. </p><p id="1426432401"> Our fundraising meeting for the year will be held tonight (Wednesday) March 18th at 6:30 pm at
Marianne and Karl Krogen’s house, 1206 East Avenue. We are always in need of great ideas, volunteers,
and more fun people to join our group….so please come if you can make it!</p><p id="1426432401">Visit our Facebook group page at Wisconsin River Trail Project to view our recent fun pictures and
keep up with our day to day fun activities, or visit our website at <a href="www.wirivertrail.org">www.wirivertrail.org</a>. Next week we
will have a big report on our DNR/DOT/ACOE/City Staff meeting being held on March 19th </p></div></div>