<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1396542458">Wisconsin Bike Trail Meeting</h1><p id="1396543074">Tuesday, April 15th at 7:00 pm</p><p id="p-1396542458">Agenda:  Choosing board members, mission statement, fundraising efforts, Non-profit Conservation Organization Status
Updates-UW-Platteville Engineering involvement, Southwest Technical College involvement, DNR/DOT grants available, Web site development/opening</p><p id="1396547660">At the Fire Station</p><div class="respond-map">
	<div id="inline-map-0" class="map-container"></div>
	<p class="map-address"><span>510 Wisconsin Ave, Boscobel, WI 53805</span> <a id="directions-0" href="http://maps.google.com/?q=510 Wisconsin Ave, Boscobel, WI 53805"><?php print _("Get Directions"); ?></a></p>
</div>
</div></div>