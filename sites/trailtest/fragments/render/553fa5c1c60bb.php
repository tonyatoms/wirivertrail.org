<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1430234565">WRT News - April 24, 2015</h1><p id="p-1430234565">I know….I know….I missed my
column last week.  But, hey!  The Price Is Right was calling!  Unfortunately my sister and I were just in
the audience, but we sure had a fun day! 
June 11th is our air date, so look for us in the third row!<o:p></o:p></p><p id="1430234925">Now on to Trail News!   Our FIRST annual yearly meeting is going to
be held the end of May.  (We’re working
on dates that all the board members can come!) 
We plan on adding new board members and reviewing our year’s progress,
as well as detailing what our next year of plans and fundraising will
entail.  The meeting will be held at 1404
Wisconsin Avenue.  We really want to
encourage all who are interested in this project to attend.  Many hands make light work, and that is true
for the WRTO.  We’re a pretty fun bunch
to hang out with!  If you are not sure if
you’re interested or not, come anyway. 
We love to see new faces!  We will
know the exact date by next week.<o:p></o:p></p><p id="1430234925">We will be sponsoring a bus trip
to The Brewers game with the Goo Goo Dolls concert afterward on Saturday,
August 15th.  The bus leaves at 12:00,
game is at 6 pm, and the concert will last about one hour after the game.  Price of the tickets and the bus ride is
$82.00.  Call 507-269-2606 for more info
or email Denise at denisefisher5995@gmail.com. 
This is going to be a BLAST!<o:p></o:p></p><p id="1430234925">The Boscobel Farmer’s Market is
a couple of weeks from becoming a reality! 
Yeah!  The WRTO is sponsoring a
booth, and will sell baked goods, plants, clothing, and merchandise, as well as
sell tickets to the Rooster Andy’s 3rd of July Chicken BBQ in
Kronshage Park.  Last year we sold 500
tickets and this year we hope to sell 600! 
Please support our local area vendors and stop in and say hi to us at
our booth!<o:p></o:p></p><p id="1430234925">Mother’s Day is coming up and
don’t forget to stop by the Timber Lane Coffee Shop for your chance to win one
of two Mother’s Day baskets!  One basket
is a wine basket, and the other is a coffee basket, donated by Marianne and
Karl Krogen and Madge Stuart and Dr. Pelz. 
The winners will be drawn on May 8th.  Tickets are just $2/ticket or 6 tickets/$10.<o:p></o:p></p><p id="1430234925">Our
big green bike is rentable for all your events, including graduations,
anniversaries, birthdays, or just to embarrass! 
Please call Joel at 375-5708 to reserve it.  The cost is $15 to put it in a yard.  If you want to move it out of your yard, it’s
$10 to move it to someone else’s yard, but $15 to just have it removed from
your yard (without putting it in your friend’s yard). </p><p id="1430234925">We
are so looking forward to June 6th from 9-noon at the Boscobel Fire
Department.  We are teaming up with the
Boscobel Fire Department, Gunderson Boscobel Hospital and Clinics, Children’s
Miracle Network, Wisconsin River Outings, and WRTO to provide
a FREE bike safety training day with our certified trainer!  What’s great is we are supplying snacks for
the kids-hot dogs, chips, granola bars, waters, and treats.  We will also have great door prizes.  Those signed up by May 22nd will
be entered into a free drawing for WRTO t-shirts and a free canoe rental!  Please email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a> or <a href="mailto:tmvial@yahoo.com">tmvial@yahoo.com</a>, or call 507-269-2606 or
608-391-0113 to be sent a registration form/permission slip.  </p><p id="1430234925">Clothing and merchandise are now
in at World of Variety!  Go check out our
selection!<o:p></o:p></p><p id="1430234925">Hope everyone has a great, great
week!  Thanks for supporting the
Wisconsin River Trail!  We’ll be digging
in the ground by fall of this year!  <o:p></o:p></p></div></div>