<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1400680577">River Trail Night at A&amp;W Family Restaurant</h1><p id="p-1400680577">June 4th from 4-8 pm
A &amp; W Family Restaurant
Boscobel, WI </p><p id="1400680717">Please stop in from 4-8pm, visit with our volunteers, board members, and sponsors. Every purchase receipt enters you for a chance to win a free prize.</p><p id="1400680717"><b> 20% of all purchases</b> will be donated to the Wisconsin River Trail Organization!</p><div class="respond-map">
	<div id="inline-map-0" class="map-container"></div>
	<p class="map-address"><span>A & W Family Restaurant Boscobel, WI </span> <a id="directions-0" href="http://maps.google.com/?q=A & W Family Restaurant Boscobel, WI "><?php print _("Get Directions"); ?></a></p>
</div>
</div></div>