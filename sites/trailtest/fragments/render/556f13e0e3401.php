<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1433342941">WRT News - May 31, 2015</h1><p id="p-1433342941">We
had our 2nd annual board meeting last Wednesday.  Thanks to all the new volunteers/interested
parties that came to find out more about our organization.  Welcome to Angie O’Brien as our newest board
member too.  We look so forward to
working together with all our volunteers to make our trail a success!  Our next meeting will be held on June 10th
at 7 pm at 1404 Wisconsin Avenue in Boscobel. 
All are welcome!</p><p id="1433343232">     June 6th
from 9-noon, bring your kids and their bikes to our FREE bike safety event.  We have a certified bicycle trainer as well
as a mechanic station for oil and air. 
Gunderson Boscobel Hospital and Clinics as well as Children’s Miracle
Network have supplied 27 new bike helmets to be given away FREE to those
children that need a new helmet.  We will
be supplying a free lunch, door prizes, and a grand prize of a free canoe
rental from Wisconsin River Outings.  Our
“graduates” will be eligible to ride in the 4th of July Parade
behind our big green bike.  The Boscobel
Fire Department and Boscobel Rescue Squad will also be assisting us.  For our registration/permission slip, please
send an email to <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>
or <a href="mailto:tmvial@yahoo.com">tmvial@yahoo.com</a>. <o:p></o:p></p><p id="1433343232"> Please visit our
booth at the Farmer’s Market for tickets to Rooster Andy’s 3rd of
July event.  For $10/person, you get ½ BBQ
chicken, potato salad, baked beans, roll, drink, and a dessert.  We will have face painting, a DJ, Schwan’s
ice cream, and some fun and games at Kronshage Park from 5-8.  We also deliver!  Tickets are on sale at all banks in Boscobel
as well as Krachey’s BP, Casey’s, Boscobel Bowl and Banquet, The Boscobel Dial,
and will also be on sale in Fennimore this next weekend as well as in Wauzeka
for their June 20th tractor ride. 
You don’t want to miss our delicious meal and fun festivities starting
out our holiday weekend in Boscobel.<o:p></o:p></p><p id="1433343232">     Also at the
weekly Farmer’s Market, we have tickets for sale for our Brewers and Goo Goo
Dolls concert on August 15th. 
For $82/person, you get in to both events, a bus ride with Boscobel’s
famous Steve Dilley as our chauffeur, and fun raffles and games on the
bus!  We can’t wait!  The bus leaves at noon from Pat and Greg’s
Pour House in Boscobel, but we will make stops in towns along the way if you
want to jump in with us.  Call Denise
Fisher at 507-269-2606 or email at <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>
to reserve your ticket!  Here’s to a
Boscobel tailgating party!  <o:p></o:p></p><p id="1433343232">     June 20th,
bring your tractor and join area farmers in driving from Wauzeka to Steuben and
back…and then more countryside driving after lunch too.  Lunch will be provided by the WRTO at Scott’s
Landing from 11-1.  Contact Tonia Vial at
391-0113 for more information.<o:p></o:p></p><p id="1433343232">Our
Father’s Day Raffle basket consists of a $40 soft side cooler, a large pizza
from Krachey’s, a 12-pack of soda, and a 12-pack of beer of your choice.  Tickets are on sale at the Farmer’s Market
and, during the week, at Timber Lane Coffee Shop for $1/each or 6
tickets/$5.00.  </p><p id="1433343232">WRTO
also has some great summer clothing for sale at the Farmer’s Market as well as
World of Variety.  Stop in and see our
weekly new clothing items.  </p><p id="1433343232">     You can donate
to the WRTO every time you order from Amazon online.  It’s easy! 
Instead of the usual website address, type in <a href="http://www.smileamazon.com/">www.smileamazon.com</a> and follow the quick
and easy instructions.  You don’t have to
pay an extra penny, and 0.5% of your order automatically goes to the WRTO.  The only glitch is that we are trying to
change the address from Los Angeles (my address) to Boscobel, and are struggling
with that….but WRTO will still get your donation.<o:p></o:p></p><p id="1433343232">Don’t forget our big green bike is available for your
family, friends, and even your “enemies” to advertise any upcoming events!  $15.00 is all it costs!  Call Joel at 375-5708.<o:p></o:p>

     Have a great
week everyone!<o:p></o:p></p></div></div>