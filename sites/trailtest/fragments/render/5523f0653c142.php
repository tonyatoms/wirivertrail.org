<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1428418664">WRT News - April 3, 2015</h1><p id="p-1428418664">We have the most adorable
winner!  David and Anne Hartberg’s
daughter was the winner of the Easter basket drawing at Timber Lane Coffee Shop
last week.  We have a picture of her on
our Facebook group page, Wisconsin River Trail Project, enjoying her new Easter
basket.  Thanks for everyone for
supporting us.  We are only just
beginning!  This month we have not one, but
TWO Mother’s Day baskets at Madge’s Timber Lane Coffee Shop!  One basket has TWO bottles of wine in it and
the other is a Coffee themed basket. 
Tickets are $2/one ticket or $10/6 tickets.  Thanks to Marianne Krogen and Madge Stuart
for donating the baskets!<o:p></o:p></p><p id="1428418890">We have begun selling merchandise at World of Variety in
Boscobel!  We have ordered many items
that are coming in daily.  We will have
t-shirts, polo shirts, Capri pants, visors, baseball caps, can coozies, window
clings, key chains, water bottles, bracelets, and draw string bags with our
logo on them.  We would like to thank the
Mudler family, Don Karasek, and the staff at World of Variety for allowing us
to display and sell our merchandise with them.<o:p></o:p></p><p id="1428418890">April 18th is Shred Day at Community First
Bank.  Don’t forget to stop by and grab a
goodie from our bake sale! If you are available to donate a baked good, please
contact any member of the Wisconsin River Trail.<o:p></o:p>

                June
6th from 9-noon is our 2nd Annual FREE Bike Safety
Training sponsored by the Gunderson Boscobel Hospital and Clinics, Children’s
Miracle Network, Boscobel Fire Department, and WRTO.  For registration forms, please contact Tonia
at 391-0113 or <a href="mailto:toniavial@yahoo.com">toniavial@yahoo.com</a> or
Denise at 507-269-2606 or <a href="mailto:denisefisher5995@gmail.com">denisefisher5995@gmail.com</a>.  We will also provide snacks and prize drawings
for the kids too</p><p id="1428418890">And
last but not least, the application for our DNR Knowles Nelson Stewardship
Grant should be arriving in the city by press time.  After a thorough review and a signature of
approval, we will know by next September if we were chosen to receive all or a
portion of our grant request.  After a
few nights of awakening and thinking we were never going to get this successfully
accomplished, we want to thank Mike Reynolds, Arlie Harris, Steve Wetter,
Jeremy Krachey, Tonia Vial and Wisconsin River Trail board members, and all the
DNR and DOT people that assisted with my first attempt at a sizable ($200,000) grant.  I couldn’t have done it without all your
help, patience, and kindness!  </p></div></div>