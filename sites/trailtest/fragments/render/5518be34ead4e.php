<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1427684922">WRT News - March 29, 2015</h1><p id="p-1427684922">Wisconsin River Trail would like to thank Gunderson Boscobel Hospital and Clinics and Children’s
Miracle Network for awarding us with a $500 helmet grant this past week. We are holding a FREE bike
safety class at 9 am on June 6thwith a certified bike trainer. We will be able to help kids that don’t have
helmets in fitting them with a new helmet properly, up to our $500 grant. We will have free drawings
and snacks for the kids. To fill out a permission/information slip or find out more details, please call
Tonia Vial at 391-0113 or Denise Fisher at 507-269-2606. We look so forward to training our local bike
riders and starting their summer off with freshening up their bike safety skills!</p><p id="1427685166">April 18th the Wisconsin River Trail is having a bake sale for Community First Bank’s Shred Day. If
you would like to donate a baked good to assist us in fundraising, please give Tonia or Denise a call.
There’s nothing like a little chocolate or wonderful homemade muffin to start your Saturday morning
out right!  </p><p id="1427685166">This past couple of weeks the DOT, DNR, city staff, and our engineer, Jeremy Krachey, have been
ironing out the details to our plans for the trail to the boat landing. The best thing you can do for our
DNR grant that we are applying for is to fill out a letter of support to send with our grant. Every letter
will be included and appreciated. Please drop your letter off at city hall between now and April 15th
.
Don’t forget to include what the trail means to you personally and our community. You may address
your letter to DNR Knowles-Nelson Stewardship Committee Members.</p><p id="1427685166">The WRTO has a SWTC student joining in our fundraising and organization group. Sarah Pitzer
Frey will be assisting us through June in creating some fun fundraising ideas. We look forward to your
help, Sarah, and would like to welcome you to our group! Thanks for choosing us for your SWTC
project!</p><p id="1427685166">Have a great week everyone!</p></div></div>