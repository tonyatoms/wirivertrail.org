<div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1416230175">501c3</h1><p id="p-1416230175">Bylaws and articles of incorporation.</p><p id="1416230497"><br></p>

<p class="file">
	<i class="fa fa-file-text-o"></i>
	<a href="../files/Bike Trail-ArticlesofAssociation.pdf"><?php print _("Download Bike Trail-ArticlesofAssociation.pdf"); ?></a>
</p>

<p class="file">
	<i class="fa fa-file-text-o"></i>
	<a href="../files/Trail_Bylaws.pdf"><?php print _("Download Trail_Bylaws.pdf"); ?></a>
</p></div></div>