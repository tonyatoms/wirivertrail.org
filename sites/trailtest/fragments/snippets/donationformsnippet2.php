      <script>

        var $amount = 0;  
        var $amountFormatted = ''; 

        function validateInput(){

            if(($amount > 0) && (! isNaN($amount))) {
                $amountFormatted = $amount + '.00';
                
                $('#inputError').hide();
                $('#readyButtonDiv').hide();
                $('#checkoutDiv').show();
            }
            else {
                $('#readyButtonDiv').show();
                $('#inputError').show();
            }        
        }      

        function amountChanged(){

            $amount =  $( "#amount" ).val() ;  

            validateInput();
        }

	</script> 

    <form id="inputForm" class="well" action='' method="POST">  

        <label>One Time Donation Amount  $</label>  

        <input class="span3" placeholder="100" type="text" onblur="amountChanged()" id="amount" name="amount"  required>
        <div hidden="true" id="inputError" style="color:red; margin-left:60px" >please enter a whole dollar amount only</div>  

        <div id="readyButtonDiv">     
            <button type="button" class="btn btn-primary">
                <span class="glyphicon glyphicon-ok"></span> next
            </button>
        </div> 

        <script src="https://checkout.stripe.com/checkout.js"></script>    

        <div id="checkoutDiv"   hidden="true">    
            <button id="customButton">Make a one time Donation!</button>
        </div>
        <script>
            var handler = StripeCheckout.configure({
                key: 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
                image: "files/t_wrtp_demo5.jpg",
                token: function(token, args) {
          // Use the token to create the charge with a server-side script.
          // You can access the token ID with `token.id`
                }
            });

            document.getElementById('customButton').addEventListener('click', function(e) {
                // Open Checkout with further options
                handler.open({
                    name: 'River Trail Project',
                    description: 'One time Donation: $' + $amountFormatted ,
                    amount: $amount * 100
                });
                e.preventDefault();
            });
        </script>        

        <hr>
      <p class="lead">  OR sign up for a monthly recurring donation at one of these levels:<p />
<script
  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
  data-key="pk_test_1MCYLYHQDa4DwnBoKd5CqoaP"
  data-image="files/t_wrtp_demo5.jpg"
  data-name="River Trail Project"
  data-description="Subscription ($5 per month)"
  data-panel-label="Subscribe"
  data-label="Subscribe for $5 Month"
  data-amount="500"
  data-allow-remember-me="false">
</script>
<script
  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
  data-key="pk_test_1MCYLYHQDa4DwnBoKd5CqoaP"
  data-image="files/t_wrtp_demo5.jpg"
  data-name="River Trail Project"
  data-description="Subscription ($10 per month)"
  data-panel-label="Subscribe"
  data-label="Subscribe for $10 Month"
  data-amount="1000"
  data-allow-remember-me="false">
</script>   
<script
  src="https://checkout.stripe.com/checkout.js" class="stripe-button"
  data-key="pk_test_1MCYLYHQDa4DwnBoKd5CqoaP"
  data-image="files/t_wrtp_demo5.jpg"
  data-name="River Trail Project"
  data-description="Subscription ($15 per month)"
  data-panel-label="Subscribe"
  data-label="Subscribe for $15 Month"
  data-amount="1500"
  data-allow-remember-me="false">
</script> 

</form>     

