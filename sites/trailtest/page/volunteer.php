<?php 
$rootPrefix="../";
$pageUrl="page/volunteer";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="5339d0edb68b6";
$pageFriendlyId="volunteer";
$pageTypeUniqId="5339d0edb65ff";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Volunteer</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="5339d0edb68b6" data-pagefriendlyid="volunteer" data-pagetypeuniqid="5339d0edb65ff" data-api="http://atoms.net/wirivertrail-dev" id="volunteer">

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li class="active"><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h1 id="h1-1"><?php print _("Volunteer!"); ?></h1><p id="paragraph-1"><?php print _("Thanks so much for visiting our volunteering page. As you know, our entire organization is made up of volunteers. We really need your support in order to keep this project thriving. There are many needs for all types of volunteers, like people willing to address and mail out brochures and donation requests, people willing to go door-to-door if needed, others willing to make phone calls, as well as those that can help with fundraising events such as brat stands or delivery of pizzas."); ?></p><p id="paragraph-2"><?php print _("There’s room for everyone willing to reach out and help. If you are willing to donate your time at some point, please send a quick email to denisefisher5995 @ gmail.com (remove the spaces) or send a note out on this website via the <a href=\"../page/contact\">Contact Form</a>. We won’t ask you to do anything you don’t want to and we are happy to wait until the right volunteering position is open at the right time for you. So no worries….and thanks for your extra commitment to our Wisconsin River Trail project!"); ?></p><h2 id="volunteer-h2-5"><?php print _("Volunteer List"); ?></h2><p id="1399988137"><?php print _("A huge thank you goes out to all that have helped or offered to help in any way they could....Let's keep our list growing....We've got a great group of friends in our town!"); ?></p><ul id="volunteer-ul-5"><li><?php print _("Jake Bacon"); ?></li><li><?php print _("Robin Baumeister"); ?></li><li><?php print _("Becky Becker"); ?></li><li><?php print _("Tom and Barb Bell"); ?></li><li><?php print _("Jordan &amp; Natosha Borowski"); ?></li><li><?php print _("Tony and Kara Breems"); ?></li><li><?php print _("Darlene Coffey"); ?></li><li><?php print _("Karen Cooley"); ?></li><li><?php print _("Mrs. Paula Davis"); ?></li><li><?php print _("Kim Drake"); ?></li><li><?php print _("Roddy Dull"); ?></li><li><?php print _("Wayne and Sue Elliott"); ?></li><li><?php print _("Steve Elliott"); ?></li><li><?php print _("Lisa Friar"); ?></li><li><?php print _("Tyler Herman"); ?></li><li><?php print _("Tim &amp; Lisa Jacobson"); ?></li><li><?php print _("Gary &amp; Diane Johnson"); ?></li><li><?php print _("Marsha Jones"); ?></li><li><?php print _("Crystal Krachey"); ?></li><li><?php print _("Jeff and Mary Lee"); ?></li><li><?php print _("Gary &amp; Diane Johnson"); ?></li><li><?php print _("Brenda Kreul"); ?></li><li><?php print _("Karl and Marianne Krogen"); ?></li><li><?php print _("Doyle Lewis"); ?></li><li><?php print _("Joel  And Karen Leonard"); ?></li><li><?php print _("Carrie Lessard"); ?></li><li><?php print _("Mac McMullen"); ?></li><li><?php print _("Bomber and Laura Merwin"); ?></li><li><?php print _("Angie O'Brien"); ?></li><li><?php print _("Dr. Tom Pelz and Madge Stewart"); ?></li><li><?php print _("Frank and Donna Phalin"); ?></li><li><?php print _("Pat and Stacey Phalin"); ?></li><li><?php print _("Barb Puckett"); ?></li><li><?php print _("Kelly Randall"); ?></li><li><?php print _("Deb Robertson"); ?></li><li><?php print _("Angela Sailer"); ?></li><li><?php print _("Meredith Sime"); ?></li><li><?php print _("Jo and Craig Sommers"); ?></li><li><?php print _("Wendi Stitzer"); ?></li><li><?php print _("Levi and Tonia Vial"); ?></li><li><?php print _("Karen Wacker"); ?></li><li><?php print _("Fred Waltz"); ?></li><li><?php print _("Sally Waltz"); ?></li></ul><h2 id="volunteer-h2-4"><?php print _("Business Supporters"); ?></h2><ul id="volunteer-ul-19"><li><?php print _("American Signs"); ?></li><li><?php print _("Baker Mill Works"); ?></li><li><?php print _("<a href=\"http://www.swnews4u.com\">Boscobel Dial</a>"); ?></li><li><?php print _("Casey's"); ?></li><li><?php print _("Community First Bank"); ?></li><li><?php print _("Fennimore Times"); ?></li><li><?php print _("Grant County Chapter of Thrivent Financial"); ?></li><li><?php print _("Krachey's BP"); ?></li><li><?php print _("Krachey's A &amp; W"); ?></li><li><?php print _("<a href=\"http://krogens.doitbest.com/home.aspx\">Krogen's How-To Store</a>"); ?></li><li><?php print _("Kwik Trip"); ?></li><li><?php print _("Lacrosse Gunderson Lutheran Hospital"); ?></li><li><?php print _("Prairie Courier Press"); ?></li><li><?php print _("Superior Trophies"); ?></li><li><?php print _("Supplemental Funds"); ?></li><li><?php print _("Trendsetters"); ?></li><li><?php print _("Unique Café<br>"); ?></li></ul></div></div>
</div>

<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
              <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <!--<div class="col-md-12 menu">
              
              <ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li class="active "><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>
              
          </div>-->
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>