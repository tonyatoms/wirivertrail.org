<?php 
$rootPrefix="../";
$pageUrl="page/board-members";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="548d310c4f368";
$pageFriendlyId="board-members";
$pageTypeUniqId="5339d0edb65ff";
$language="en-us";
include '../libs/Utilities.php';
include '../libs/SiteAuthUser.php';
include '../site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - Board Members</title>
<meta name="description" content="<?php print _("Wisconsin River Trail Board Members"); ?>">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="../css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="../css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="548d310c4f368" data-pagefriendlyid="board-members" data-pagetypeuniqid="5339d0edb65ff" data-api="http://atoms.net/wirivertrail-dev" id="board-members">

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="../files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><h3 id="board-members-h3-1" class="text-center"><?php print _("Wisconsin River Trail Board Members"); ?></h3><p id="1418542515"><?php print _("Under Construction"); ?></p><table id="board-members-table-2" class="table table-striped table-bordered col-2" data-columns="2"><thead><tr><th class="col-1"></th><th class="col-2"><?php print _("Member"); ?></th></tr></thead><tbody><tr class="row-1"><td class="col-1"></td><td class="col-2"><?php print _("<i><b>Denise Elliott Fisher</b></i> -<i style=\"line-height: 20.4285717010498px;\"><b>- </b>Is a native of Boscobel, Wisconsin. One cold day in January of 2014, she spoke her dreams out load in Timber Lane Coffee Shop in Boscobel, and that dream is heading to a reality! Denise is a nurse by trade, but has chosen to volunteer her work towards this trail as a full time donation. She lives the warm half of the year in Boscobel, and the cold half of the year in Los Angeles,CA, with her very understanding husband Dean.They have four grown children: Kelsey,Sami,Jay and Brady, and a doggie Gabby;that loves her porch in Boscobel! Denise's inspiration is to ride the completed trail with her grandchildren one day (although there are none to date...yet!).</i>"); ?></td></tr><tr class="row-2"><td class="col-1"><?php print _("<img id=\"image5\" src=\"http://atoms.net/wirivertrail-dev2/sites/trailtest/files/Joel.jpg\">"); ?></td><td class="col-2"><?php print _("<i style=\"line-height: 20.4285717010498px;\"><b>Joel Leonard - </b></i><i style=\"line-height: 20.4285717010498px;\">Is retired having taught English and coached for 31 years, 30 of them in Boscobel. He also served as a computer coordinator and activities director for the district. In addition he served two terms as the association president where he helped to organize the first BEA Sander's Creek Clean-up. He also helped to charter a very successful Jaycee Chapter in Boscobel during the 80s and 90s while serving as president. Joel has been married to his wife Karen for over 40 years and has a very environmentally active daughter, Katya, who is employed at Organic Valley, Joel and Karen are the grandparents of a vivacious granddaughter, Anya. Joel is excited to be involved in this grand project that will benefit Boscobel and the surrounding area.</i>"); ?></td></tr><tr class="row-3"><td class="col-1"><?php print _("<img id=\"image1\" src=\"http://atoms.net/wirivertrail-dev2/sites/trailtest/files/IMG_20150220_144325%20(Mobile).jpg\">"); ?></td><td class="col-2"><?php print _("<b style=\"font-style: italic;\">Tonia Atkinson Vial - </b> grew<i style=\"font-size: 14.3000001907349px; line-height: 20.4285717010498px;\"> up in Wauzeka-Steuben school district. Has lived in Boscobel since 1998 with her husband and kids. Tonia works for the family excavating and trucking business located in Wauzeka. since 1997 . She loves to help and donate time to her surrounding communities in activities from charities to volunteering for school events and  school needs. She runs and organizes run\walks in the community. She has raised funds and participated  numerous times in a 3 -Day 60mi walk for \"Relay to Live\" event with groups of 2-10 individuals. Tonia desires that her children grow up staying active, healthy and community supportive.</i>"); ?></td></tr><tr class="row-4"><td class="col-1"><?php print _("<img id=\"image1\" src=\"http://atoms.net/wirivertrail-dev2/sites/trailtest/files/1972517_339597712881303_9119530481200306810_n.jpg\">"); ?></td><td class="col-2"><?php print _("<i><b>Mactire Armstrong McMullen - </b>Has been involved in cycling since the tender age of seven in Southern California. While others his age were playing in sandlots or on the beach, Mactire was learning and gaining his passion for cycling from that era of cyclists. By age nine he left the neighborhood riding behind and hit the roads of Southern California with his mentors. He has been involved with numerous bike clubs across the nation and previous owner and current admin of  a cycling forum.  He currently resides in Spring Green, WI with his wife Connie. They have three grown children. In his professional career Mactire is an IT Programmer. Outside of cycling his other passions include photography and Scots\Irish genealogy. Mac is a member of League of American Bicyclists and supporter of Rails-To-Trails.</i>"); ?></td></tr><tr class="row-5"><td class="col-1"></td><td class="col-2"><?php print _("<i><b>Wendi Stitzer</b></i>"); ?></td></tr><tr class="row-6"><td class="col-1"></td><td class="col-2"><?php print _("<i><b>Jo Summers</b></i>"); ?></td></tr></tbody></table><p id="board-members-paragraph-2" class="text-center"><?php print _("Please contact a board member for further information about our organization or concerns."); ?></p></div></div>
</div>

<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
             <a href="../page/501c3"><?php print _("501-(c)(3) info"); ?></a>
             <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="../page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
            <!-- Commented out above as not needed with menu item  and activate items below (Mactire)-->
            <!--<p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
              <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
              <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <!--<div class="col-md-12 menu">
              
              <ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="../index">Home</a></li><li><a href="../news">News</a></li><li class="dropdown"><a href="../" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/volunteer">Volunteer</a></li><li><a href="../donate">Donate</a></li><li><a href="../page/events">Events</a></li><li><a href="../merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="../page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/board-members">Board Members</a></li><li><a href="../page/contact">Contact</a></li><li><a href="../page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="../group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="../trail-map">Trail Map</a></li><li class="dropdown"><a href="../page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="../page/directions">Directions</a></li><li><a href="../page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>
              
          </div>-->
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="../js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="../js/prettify.js"></script>


</body>

</html>