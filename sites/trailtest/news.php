<?php 
$rootPrefix="";
$pageUrl="news";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="5339d0edb7432";
$pageFriendlyId="news";
$pageTypeUniqId="-1";
$language="en-us";
include 'libs/Utilities.php';
include 'libs/SiteAuthUser.php';
include 'site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - News</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="5339d0edb7432" data-pagefriendlyid="news" data-pagetypeuniqid="-1" data-api="http://atoms.net/wirivertrail-dev" id="news">
  
<header role="banner">
  
	<nav class="navbar navbar-default" role="navigation">
      
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="files/bikeTrailLogo129x100.jpg"></a>
        
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="index">Home</a></li><li class="active"><a href="news">News</a></li><li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/volunteer">Volunteer</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li><a href="merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/board-members">Board Members</a></li><li><a href="page/contact">Contact</a></li><li><a href="page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="trail-map">Trail Map</a></li><li class="dropdown"><a href="page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/directions">Directions</a></li><li><a href="page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
    <div id="block-1" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-12"><div id="list1" class="respond-list" data-bind="foreach: list1" 
	data-display="blog" 
	data-label="posts" 
	data-pagetypeid="5339d0edb7056" 
	data-length="10" 
	data-orderby="Created" 
	data-category="undefined">
		<div class="content" data-bind="html:content"></div>
        <div class="blog-meta">
			<p>
				<span data-bind="visible:hasPhoto"><span class="photo" data-bind="attr:{'style': 'background-image: url('+photo+')'}"></span></span>
                <?php print _("Last modified by"); ?> <span class="author" data-bind="text:author"></span>
                <span data-bind="text:lastModifiedReadable" class="last-modified-date"></span>
                <a data-bind="attr:{'href':url}"><?php print _("Permanent Link"); ?></a>
			</p>
        </div>
</div>

<p data-bind="visible: list1Loading()" class="list-loading"><i class="fa fa-spinner fa-spin"></i> Loading...</p>

	<div class="page-results">
		<button id="pager-list1" class="btn btn-default" data-id="list1"><?php print _("Older posts"); ?></button>
	</div>
</div></div>
</div>

<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
            <a href="page/501c3"><?php print _("501-(c)(3) info"); ?></a>
            <!--<h4><?php print _("Contact"); ?></h4>
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
           <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
              <!--<a href="mailto:wisrivertrail@gmail.com"><i class="fa fa-envelope-o"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>-->
			</p> 
           <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <!--<div class="col-md-12 menu">
              
              <ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="index">Home</a></li><li class="active "><a href="news">News</a></li><li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/volunteer">Volunteer</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li><a href="merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/board-members">Board Members</a></li><li><a href="page/contact">Contact</a></li><li><a href="page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="trail-map">Trail Map</a></li><li class="dropdown"><a href="page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/directions">Directions</a></li><li><a href="page/places-to-stay">Places to Stay</a></li></ul></li>
              </ul>
              
          </div>-->
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="js/prettify.js"></script>


</body>

</html>