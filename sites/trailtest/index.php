<?php 
$rootPrefix="";
$pageUrl="index";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="5339d0ed9f655";
$pageFriendlyId="index";
$pageTypeUniqId="-1";
$language="en-us";
include 'libs/Utilities.php';
include 'libs/SiteAuthUser.php';
include 'site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">  

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="css/home.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="wirivertrail.org" data-pageuniqid="5339d0ed9f655" data-pagefriendlyid="index" data-pagetypeuniqid="-1" data-api="http://atoms.net/wirivertrail-dev">
 
<header role="banner">
    
	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
        
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//wirivertrail.org"><img src="files/bikeTrailLogo129x100.jpg"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li class="active"><a href="index">Home</a></li><li><a href="news">News</a></li><li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/volunteer">Volunteer</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li><a href="merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/board-members">Board Members</a></li><li><a href="page/contact">Contact</a></li><li><a href="page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="trail-map">Trail Map</a></li><li class="dropdown"><a href="page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/directions">Directions</a></li><li><a href="page/places-to-stay">Places to Stay</a></li></ul></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>

<div id="content" class="container" role="main">
  <script type="text/javascript" src="js/custom/slidey.js"></script>
  
  <div id="carouselx" class="carousel slide"> 
    <div class="carousel-inner">    
      <div class="item active">      
        <img src="files/river_1_1200x480.jpg" alt="" />      
        <div class="carousel-caption">        
          <p><?php print _("<!-- Caption text here -->"); ?></p>      
        </div>    
      </div>    
      <div class="item">      
        <img src="files/river_3_1200x480.jpg" alt="" />      
        <div class="carousel-caption">        
          <p><?php print _("<!-- Caption text here -->"); ?></p>        
        </div>    
      </div>    
      <div class="item"><img src="files/river_2.jpg" alt="" />      
        <div class="carousel-caption">        
          <p><?php print _("<!-- Caption text here -->"); ?></p>       
        </div>    
      </div>  
      <div class="item"><img src="files/river_4_1200x480.jpg" alt="" />      
        <div class="carousel-caption">        
          <p><?php print _("<!-- Caption text here -->"); ?></p>       
        </div>    
      </div>
      <div class="item">
        <img src="files/river_5.jpg" alt="" />
      </div>
        <div class="item">
        <img src="files/river_6.jpg" alt="" />
      </div>
       <div class="item">
        <img src="files/river_7.jpg" alt="" />
      </div>
    </div><!-- /.carousel-inner -->  <!--  Next and Previous controls below        href values must reference the id for this carousel -->    
    <a class="carousel-control left" href="#carouselx" data-slide="prev">‹</a>    
    <a class="carousel-control right" href="#carouselx" data-slide="next">›</a>
  </div><!-- /.carousel -->
  <div id="block-2" class="block row" data-nested="not-nested" data-containerid="" data-containercssclass=""><div class="col col-md-4"><h2 id="h2-2"><?php print _("If We Build It... They Will Come!"); ?></h2><p id="paragraph-3"><?php print _("Welcome to Wisconsin River Trail!  We are in the process of building a bike trail that connects our little communities of Boscobel, Woodman, and Wauzeka, to the river that makes us who we are. We are excited to share every step of our way with you and our end product will be meeting you on our trail with a smile and a friendly hello."); ?></p><h2 id="index-h2-11"><?php print _("Our Mission"); ?></h2><p id="1418536702"><?php print _("<i>To preserve the beauty of our nature trails through the acquisition of property for conservation, in order to promote an environment of health and wellness.</i>"); ?></p><h2 id="index-h2-6"><?php print _("Our Goals"); ?></h2><p id="index-paragraph-5"><?php print _("As our goals are established with a “go big or go home” attitude, we are accomplishing our project in phases.  The first phase is to finish our Sanders Creek walking/biking trail to our boat landing in Boscobel. Depending on the natural habitat that exists there, the cost of fill needed, and the most recreationally beneficial boat landing to attach our trail to, we have yet to commit to a west side (Floyd Von Haden Boat Landing) or the east side, which is a more basic landing that allows for canoeists to port.  No matter what side is chosen, our citizens, children, and tourists will be able to walk or bike to the boat landing safely, viewing our natural habitat of eagle’s nests and turtle habitats as they travel."); ?></p><p id="index-paragraph-6"><?php print _("Our next goal is to create a hiking/biking trail that leads from Boscobel toward Woodman following, in areas, the railroad trails.  It is legally and financially beneficial to parallel parts of the railroad trails, 51 feet from the tracks, to minimize the need to clear land, and also, to widen easements that are not taxing to the landowners."); ?></p><p id="index-paragraph-7"><?php print _("Phase three, our biggest feat and most exciting segment of our trail, will be to cross over the Wisconsin River in an area that boasts country and river scenery that allows for peaceful, one-of-a-kind views of nature that can now only be seen by boat.  We are contracting with UW-Platteville Engineering departments to construct a bridging system that is disability accessible, with fishing areas, and even a view of an early 1900’s turnstile, built when the river was deep and wide enough for barge traffic. It is approximately 4 miles to Wauzeka from this path, with a need for 4-7 bridges, and a stretch of land and river that will bring you back to nature in a pristine, quiet, and relaxing setting."); ?></p></div><div class="col col-md-4"><h1 STYLE="color: White;">Reasons</h1><p id="index-paragraph-8"><?php print _("Our last phase will be to bring the trail full circle from Wauzeka back to Boscobel following roughly the Highway 60 path which follows the Wisconsin River."); ?></p><p id="paragraph-9"><?php print _("We know our goals are lofty, but we also know that, if we divide this trail into phases, that we will succeed and be able to share the land that we love so much, with others that may need the quiet solitude of our rural setting."); ?></p><p id="1426916001"></p><h2>Fundraising</h2><p id="index-paragraph-9"><?php print _("Our biggest need is, of course, funding. We are obtaining funding through the applications of grants and funds donated to our non-profit organization and also with our municipality’s support to secure additional funding. We have made it possible so that, if you would prefer not to donate a one-time donation, that you can contribute a smaller amount monthly or bi-weekly so as not to tax your family’s finances. Every dollar counts in our project, and all of our finances go directly to creating our legacy of a lifetime, the Wisconsin River Trail."); ?></p><p id="1426916001"></p><h2>Trail News</h2><p id="index-paragraph-10"><?php print _("Please view our <a href=\"http://atoms.net/wirivertrail-dev2/sites/trailtest/news\">NEWS</a><a href=\"/news\"> </a>section for our day-by-day challenges and feats, and to fill in the areas of questions that you may have.  Also feel free to contact us with any questions, concerns, or thoughts that you may have that might assist us in our trail blazing efforts. We appreciate your visit and hope you put us on your FAVORITES list to watch us grow."); ?></p><h2 id="index-h2-14"><?php print _("Events"); ?></h2><p>
Please view our <a href="http://www.wirivertrail.org/page/events">EVENTS</a> section for a listing of upcoming events .
<br/><br/>
<i>June 6<sup>th</sup></i> - Bicycle Safety Training</br>
<i>July 3<sup>rd</sup></i> - Rooster Andy's Chicken BBQ</br>
<i>July 4<sup>th</sup/></i> - <a href="http://www.boscobelfirecrackerrun.com/">Firecracker Run</a>
</p></div><div class="col col-md-4"><h2 id="h2-4"><?php print _("Support our Sponsors"); ?></h2><p id="1397581395"><?php print _("<img id=\"image1\" src=\"files/fake_ad1_300x100.jpg\">"); ?></p><p id="paragraph-5"><?php print _("This space is reserved for our generous sponsors who support our work by buying advertising space on this web site.  Businesses, organizations and individuals are welcome to buy ads here. <a href=\"page/contact\">Contact us</a> to reserve your ad, and <b>thanks!</b>"); ?></p><a href="http://krogens.doitbest.com/"Krogen's</a><div id="imagecontainer1" class="o-image"><img id="image1" src="files/1384074_599136213476432_9536212_n.png"></div></div></div>
</div>
<script type="text/javascript" src="js/custom/facebookfeed.js"></script>
<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-4">
           <a href="page/501c3"><?php print _("501-(c)(3) info"); ?></a>
            
            <!--<h4><?php print _("Contact"); ?></h4>
            
            <p>
             <?php print _("Drop us a line via the:"); ?> <a href="page/contact"><?php print _("Contact Form"); ?></a>
            </p>-->
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <!--<h4><?php print _("About"); ?></h4>-->
             <p class="social">
              <a href="https://www.facebook.com/groups/Wisconsinrivertrailproject/"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/WisRiverTrail"><i class="fa fa-twitter"></i></a>
            </p>
            <p>
              Wisconsin River Trail Project<?php print _(" site "); ?> <br/> by <a href="http://atoms.net"> atoms.net</a>. 
               Copyright © <!--2014–--><script type="text/javascript">
                                        var theDate = new Date(); document.write(theDate.getFullYear())
              </script> 
            </p>
            
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <ul>
              	<!--<li><?php print _("Menu:"); ?></li>-->
                <!--<li class="active "><a href="index">Home</a></li><li><a href="news">News</a></li><li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/volunteer">Volunteer</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li><a href="merchandise">Merchandise</a></li></ul></li><li class="dropdown"><a href="page/board-members" class="dropdown-toggle" data-toggle="dropdown">Board Members <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/board-members">Board Members</a></li><li><a href="page/contact">Contact</a></li><li><a href="page/contact-site-support">Contact Site Support</a></li></ul></li><li class="dropdown"><a href="group-links" class="dropdown-toggle" data-toggle="dropdown">Group Links <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li><li><a href="http://www.railstotrails.org/build-trails/trail-building-toolbox/">Trail Building Toolbox</a></li><li><a href="http://wisconsinriverfriends.org/">Friends Of Lower Wisconsin Riverway (FLOW)</a></li></ul></li><li><a href="trail-map">Trail Map</a></li><li class="dropdown"><a href="page/directions" class="dropdown-toggle" data-toggle="dropdown">Visiting? <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="page/directions">Directions</a></li><li><a href="page/places-to-stay">Places to Stay</a></li></ul></li>-->
              </ul>
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>
  
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="js/prettify.js"></script>


</body>

</html>