<?php 
$rootPrefix="";
$pageUrl="testdonate";
$isSecure=false;
$siteUniqId="5339d0ed981eb";
$siteFriendlyId="trailtest";
$pageUniqId="53581e67d191b";
$pageFriendlyId="testdonate";
$pageTypeUniqId="-1";
$language="en-us";
include 'libs/Utilities.php';
include 'libs/SiteAuthUser.php';
include 'site.php';
?><!doctype html>

<html lang="<?php print $language; ?>">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Wisconsin River Trail Project - testdonate</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="callout" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<!-- css -->
<link href="css/content.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.2/css/font-awesome.css" rel="stylesheet">
<link href="css/prettify.css?v=2.9.1" type="text/css" rel="stylesheet" media="screen">

    
</head>

<body data-siteuniqid="5339d0ed981eb" data-sitefriendlyid="trailtest" data-domain="atoms.net/wirivertrail-dev/sites/trailtest" data-pageuniqid="53581e67d191b" data-pagefriendlyid="testdonate" data-pagetypeuniqid="-1" data-api="http://atoms.net/wirivertrail-dev" id="testdonate">

<header role="banner">

	<nav class="navbar navbar-default" role="navigation">
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        
        <a class="navbar-brand" href="//atoms.net/wirivertrail-dev/sites/trailtest"><img src="files/wrtp-logo.png"></a>
	  </div>
	  <!-- /.navbar-header -->
	
	  
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
			<li><a href="index">Home</a></li><li><a href="blog">Blog</a></li><li><a href="page/about">About</a></li><li><a href="page/contact">Contact</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li class="dropdown"><a href="local-groups" class="dropdown-toggle" data-toggle="dropdown">Local Groups <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li></ul></li><li><a href="merchandise">Merchandise</a></li><li><a href="trail-map">Trail Map</a></li>
          	<li class="nav-search dropdown"> <!-- .open to show -->
	<form class="respond-search" data-for="resultsmenu">
      <div class="input-group">
        <input type="text" class="form-control">
        <button class="input-group-addon"><i class="fa fa-search"></i></button>
      </div>  
    </form>  
  	<ul class="dropdown-menu">
	  	<li class="searching"><i class="fa fa-spinner fa-spin"></i> <?php print _("Searching..."); ?></li>
	  	<li class="no-results"><?php print _("No results found"); ?></li>
  	</ul>
</li>
	    </ul>
        
	  </div>
	  <!-- /.navbar-collapse -->
      
      
	</nav>

</header>
  
<div id="content" class="container" role="main">
          <script>

        var $amount = 0;  
        var $amountFormatted = ''; 
        var checkboxVal = 'off';    
        var checkboxValFormatted = 'One Time Donation $';    


        function validateInput(){

            if(($amount > 0) && (! isNaN($amount))) {
                $amountFormatted = $amount + '.00';
                
                if($("#subscriptionCbox").is(':checked')) {
                    checkboxValFormatted = 'Monthly Subscription of $'; 
                }
                else {
                    checkboxValFormatted = 'One time Donation: $';
                }
                $('#inputError').hide();
                $('#readyButtonDiv').hide();
                $('#checkoutDiv').show();
            }
            else {
                $('#readyButtonDiv').show();
                $('#checkoutDiv').hide();
                $('#inputError').show();
            }        
        }      


        function checkboxChanged(){

            validateInput();

            checkboxVal = document.getElementById("subscriptionCbox").value;
        }

        function amountChanged(){

            $amount =  $( "#amount" ).val() ;  

            validateInput();
        }

	</script> 

    <form id="inputForm" class="well" action='' method="POST">  

        <label>Donation Amount  $</label>  

        <input class="span3" placeholder="100" type="text" onblur="amountChanged()" id="amount" name="amount"  required>
        <div hidden="true" id="inputError" style="color:red; margin-left:60px" >please enter a whole dollar amount only</div>  

        <label class="checkbox">  
        <input type="checkbox" id="subscriptionCbox" onchange="checkboxChanged()"> Make this a recurring (monthly) donation! 
        </label>     

        <div id="readyButtonDiv">     
            <button type="button" class="btn btn-primary">
                <span class="glyphicon glyphicon-ok"></span> next
            </button>
        </div> 

        <script src="https://checkout.stripe.com/checkout.js"></script>    

        <div id="checkoutDiv"   hidden="true">    
            <button id="customButton">Donate!</button>
        </div>
        <script>
            var handler = StripeCheckout.configure({
                key: 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
                image: "files/t_wrtp_demo5.jpg",
                token: function(token, args) {
          // Use the token to create the charge with a server-side script.
          // You can access the token ID with `token.id`
                }
            });

            document.getElementById('customButton').addEventListener('click', function(e) {
                // Open Checkout with further options
                handler.open({
                    name: 'River Trail Project',
                    description: checkboxValFormatted + $amountFormatted ,
                    amount: $amount * 100
                });
                e.preventDefault();
            });
        </script>        
    </form>     
 
</div>

<footer role="contentinfo">
  
  <div class="container">

		<div class="row">
		
          <div class="col-md-6">
            
            <h4><?php print _("Contact"); ?></h4>
            
            <p>
              <?php print _("Call us at (555) 555-5555 or reach out via the website:"); ?> <a href="page/contact"><?php print _("Contact"); ?></a>
            </p>
            
            <p class="social">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-google-plus"></i></a>
              <a href="#"><i class="fa fa-envelope-o"></i></a>
            </p>
            
          </div>
          <!-- /.col-md-4 -->
          
          <div class="col-md-6">
            
            <h4><?php print _("About"); ?></h4>
            
              <p>
              Wisconsin River Trail Project <?php print _("site by"); ?> <a href="http://atoms.net">atoms.net</a>.  
            </p>
          </div>
			<!-- /.col-md-4 -->
			
		</div>
		<!-- /.row -->
    
    	<div class="row">
        
          <div class="col-md-12 menu">
              
              <ul>
                <li><?php print _("Menu:"); ?></li>
                <li><a href="index">Home</a></li><li><a href="blog">Blog</a></li><li><a href="page/about">About</a></li><li><a href="page/contact">Contact</a></li><li><a href="donate">Donate</a></li><li><a href="page/events">Events</a></li><li class="dropdown"><a href="local-groups" class="dropdown-toggle" data-toggle="dropdown">Local Groups <b class="caret"></b></a><ul class="dropdown-menu"><li><a href="http://www.hikebikewisconsin.com">Hike Bike Wisconsin</a></li><li><a href="http://canoe-camping.com/contact/boscobel-hq">Wisconsin Canoe Camping</a></li></ul></li><li><a href="merchandise">Merchandise</a></li><li><a href="trail-map">Trail Map</a></li>
              </ul>
              
          </div>
          <!-- /.col-md-12 -->
          
    	</div>
		
  	</div>
  	<!-- /.container -->
  
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment-with-langs.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWhY0ShBGeUOvjXl8s4Uie4ByWl_SvxiY&sensor=false"></script>
<script type="text/javascript" src="js/respond.Map.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Form.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Calendar.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.List.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Featured.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Login.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Registration.js?v=2.9.1"></script>
<script type="text/javascript" src="js/respond.Search.js?v=2.9.1"></script>
<script type="text/javascript" src="js/pageModel.js?v=2.9.1"></script>
<script type="text/javascript" src="js/prettify.js"></script>


</body>

</html>